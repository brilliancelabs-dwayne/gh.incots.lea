<?php
/**
** Copyright 2011-2012, Brilliance Labs LLC.
** Author: Dwayne Ford
** Project: Lea
** Version: 1.0
**/

class AppController extends Controller {

	var $components = array('Auth', 'Session');

	var $helpers = array('Html', 'Form', 'Session',
        //Helper for playing various media files like YouTube videos
        'Media',
        );

	var $uses = array('Partner');

	var $user = array();

	function beforeFilter(){

		$user = $this->Auth->User();

		$user = $user['User'];

		//save the server to the Auth User data
		if(isset($_SERVER['HTTP_HOST'])){
			$host = $_SERVER['HTTP_HOST'];
		}else{
			$host = 'localhost';
		}
		$user['host'] = $host;

		$this->user = $user;

		$p = $this->Partner->find('list');

		$partner = "incots";

		if(isset($user) && isset($user['partner_id'])):

			$partner = $p[$user['partner_id']];


		endif;

		//Restrict by IP
		$client_ip = gethostbyname($_SERVER['HTTP_HOST']);
		//debug( $client_ip );
		//debug( $_SERVER['REMOTE_ADDR'] );
		$this->set(compact('user', 'partner'));


		//$this->Auth->loginAction = array('admin' => false, 'controller' => 'partners', 'action' => 'login');
		//$this->Auth->allow('*');
		//$this->Auth->fields = array('username' => 'Trainee', 'password' => 'PassCode');
	

	}

}