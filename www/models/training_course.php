<?php
class TrainingCourse extends AppModel {
	var $name = 'TrainingCourse';
	var $displayField = 'Code';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasOne = array(
		'TrainingCourseMap' => array(
			'className' => 'TrainingCourseMap',
			'foreignKey' => 'training_course_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	var $hasMany = array(
		'TrainingCourseCompletion' => array(
			'className' => 'TrainingCourseCompletion',
			'foreignKey' => 'training_course_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'TrainingCourseEnrollment' => array(
			'className' => 'TrainingCourseEnrollment',
			'foreignKey' => 'training_course_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'TrainingCourseModule' => array(
			'className' => 'TrainingCourseModule',
			'foreignKey' => 'training_course_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Registration' => array(
			'className' => 'Registration',
			'foreignKey' => 'FirstCourse',
			'dependent' => false
	
		)
	);

}
