<?php
class EmploymentPref extends AppModel {
	var $name = 'EmploymentPref';
	var $displayField = 'Name';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'Registration' => array(
			'className' => 'Registration',
			'foreignKey' => 'employment_pref_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
