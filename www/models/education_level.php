<?php
class EducationLevel extends AppModel {
	var $name = 'EducationLevel';
	var $displayField = 'Name';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'Registration' => array(
			'className' => 'Registration',
			'foreignKey' => 'education_level_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
