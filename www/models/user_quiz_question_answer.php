<?php
class UserQuizQuestionAnswer extends AppModel {
	var $name = 'UserQuizQuestionAnswer';
	var $displayField = 'id';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'QuizResource' => array(
			'className' => 'QuizResource',
			'foreignKey' => 'quiz_resource_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'QuizQuestion' => array(
			'className' => 'QuizQuestion',
			'foreignKey' => 'quiz_question_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
