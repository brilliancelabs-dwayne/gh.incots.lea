<?php
class UserNoticeResponse extends AppModel {
	var $name = 'UserNoticeResponse';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'NoticeResource' => array(
			'className' => 'NoticeResource',
			'foreignKey' => 'notice_resource_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
