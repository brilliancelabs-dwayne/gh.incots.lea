<?php
class RegistrationsController extends AppController {

	var $name = 'Registrations';

	function beforeFilter(){
		parent::beforeFilter();

		$this->Auth->allow('*');

		$this->loadModel('TrainingCourse');
		$first_courses = $this->TrainingCourse->find('list');
		//debug($first_courses);
		$this->set(compact('first_courses'));
	}














/*Baked Goods*/
	function index() {
		$this->Registration->recursive = 0;
		$this->set('registrations', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid registration', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('registration', $this->Registration->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Registration->create();
			if ($this->Registration->save($this->data)) {
				$this->Session->setFlash(__('The registration has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The registration could not be saved. Please, try again.', true));
			}
		}
		$partners = $this->Registration->Partner->find('list');
		$users = $this->Registration->User->find('list');
		$employmentPrefs = $this->Registration->EmploymentPref->find('list');
		$educationLevels = $this->Registration->EducationLevel->find('list');
		$this->set(compact('partners', 'users', 'employmentPrefs', 'educationLevels'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid registration', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Registration->save($this->data)) {
				$this->Session->setFlash(__('The registration has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The registration could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Registration->read(null, $id);
		}
		$partners = $this->Registration->Partner->find('list');
		$users = $this->Registration->User->find('list');
		$employmentPrefs = $this->Registration->EmploymentPref->find('list');
		$educationLevels = $this->Registration->EducationLevel->find('list');
		$this->set(compact('partners', 'users', 'employmentPrefs', 'educationLevels'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for registration', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Registration->delete($id)) {
			$this->Session->setFlash(__('Registration deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Registration was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
