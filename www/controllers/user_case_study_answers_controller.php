<?php

	class UserCaseStudyAnswersController extends AppController {
		
		var $name = 'UserCaseStudyAnswers';

		function beforeFilter(){
			parent::beforeFilter();
			

			$this->layout = 'ajax';

		}	


		function mark_done($action = null, $resource_id = null){

			$this->autoRender = false;

			if($this->data):
			
				//Override the resource id
				$resource_id = $this->data['UserCaseStudyAnswer']['case_study_resource_id'];

				//Set the entered answer to a variable
				$answer = $this->data['UserCaseStudyAnswer']['Answer'];


				//Grab the next element for routing
				$next = $this->data['UserCaseStudyAnswer']['Next'];

				//Does the value already exist?
					//initialize found as no
					$found = 0;

				$find = $this->UserCaseStudyAnswer->find('first', array('conditions' => array(
					'user_id' => $this->user['id'],
					'case_study_resource_id' => $resource_id
				)));

				//format the input data
				$data = array(
						'user_id' => $this->user['id'],
						'case_study_resource_id' => $resource_id,
						'Answer' => $answer, //this needs to be the real answer from the page
						'Updated' => date('Y-m-d H:i:s')

				);

				$this->data = $data;
	

				if(!empty($find)){
				
					//compare the new answer to the answer that is currently loaded
					if($answer == $find['UserCaseStudyAnswer']['Answer']){
						//if the answer is the same
						$found = 1;
					}else{
						//if the answers are different then allow for an update
						$found = 0;
					}
					
				}

				//only make an insertion if the value is not already found
				if(!$found):

					//Save the info in the database
					$this->UserCaseStudyAnswer->save($this->data);

				endif;

				//mark the training course map element as done
				
				//re-route despite the find condition
				$this->redirect('/train/'.$next);

			endif;

		}
		
	}
