<?php
	class PartnerTraineesController extends AppController {
		var $name = 'PartnerTrainees';
		var $uses = array('PartnerTrainee', 'Partner');

		function beforeFilter(){
			$this->Auth->allow('*');
		}

		function show($partner = null){

			$this->layout = 'ajax';

			if($partner):
				$trainee_list  = $this->PartnerTrainee->find('list', array('conditions' => array(	
					'PartnerTrainee.partner_id' => $partner
				)));
			else:
				$trainee_list  = $this->PartnerTrainee->find('list');
			endif;
			
			echo json_encode($trainee_list);

		}


	}
