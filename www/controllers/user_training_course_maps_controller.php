<?php
class UserTrainingCourseMapsController extends AppController {

	var $name = 'UserTrainingCourseMaps';

	var $uses = array('UserTrainingCourseMap', 'TrainingCourseMap', 'TrainingCourse');


	function beforeFilter(){

		parent::beforeFilter();



	}

	function __exists($training_course_map_id = null){

		if($training_course_map_id):

			//Check to see if the record exists
			$exists = $this->UserTrainingCourseMap->find('first', array('conditions' => array(
				'UserTrainingCourseMap.user_id' => $this->user['id'],
				'UserTrainingCourseMap.training_course_map_id' => $training_course_map_id

			)));

			if(empty($exists)){
				//If the above query is empty then there is no value in the db
				return false;

			}else{
				//The result is not empty so there is a value in the db
				return true;
			}

		endif;

	}//end f(x)

	function __getId($training_course_map_id = null){

		if($training_course_map_id):

			$getId = $this->UserTrainingCourseMap->find('first', array('conditions' => array(
				'UserTrainingCourseMap.user_id' => $this->user['id'],
				'UserTrainingCourseMap.training_course_map_id' => $training_course_map_id
			)));

			if(!empty($getId) && isset($getId['UserTrainingCourseMap']['id'])){
				return $getId['UserTrainingCourseMap']['id'];

			}


		endif;
	}//end f(x)

	function update($id = null, $status = null, $notes = null){
		$this->__update($id, $status, $notes);

	}

	function __update($id = null, $status = null, $notes = null){

		$this->layout = 'ajax';

		if($id):	
			
			//Update the Status and Updated timestamp
			$this->UserTrainingCourseMap->read(null, $id);

			if(!$notes):
						$this->UserTrainingCourseMap->set(array(
							'Status' => $status,
							'Updated' => date('Y-m-d H:i:s')
						));
			else:
						$this->UserTrainingCourseMap->set(array(
							'Status' => $status,
							'Notes' => $notes,
							'Updated' => date('Y-m-d H:i:s')
						));
			endif;

			if( $this->UserTrainingCourseMap->save() ){
				$this->Session->setFlash('Successful Update');


			}


		endif;
	}//end f(x)

	function init($course_id = null){

		//Create course ID from form data if available
		if(!$course_id && $this->data){

			if(isset($this->data['UserTrainingCourseMaps']['courses'])){
				$course_id = $this->data['UserTrainingCourseMaps']['courses'];
				
			}
		}

		//intialize the requested course for the logged in user
		if($course_id):
			
			$find = $this->TrainingCourseMap->find('all', array('conditions' => array(
				'TrainingCourseMap.training_course_id' => $course_id
	
			)));

		
			//Format the map for saving
			$this->data = array(); //initialize
			$data = array(); //initialize
			$i = 0;

			foreach($find as $f):

				if( !$this->__exists($f['TrainingCourseMap']['id']) ):	
					$data['user_id'] = $this->user['id'];
					$data['training_course_id'] = $course_id;
					$data['training_course_map_id'] = $f['TrainingCourseMap']['id'];
					$data['Status'] = 0; //All elements will be marked as UNDONE
					$data['Created'] = date('Y-m-d');
					$data['Updated'] = date('Y-m-d H:i:s');
				
					//Add to multi-dimensional array
					$this->data['UserTrainingCourseMap'][$i] = $data;
					
					$this->data['UserTrainingCourseMap'] = $data;
					$this->UserTrainingCourseMap->create();
					$this->UserTrainingCourseMap->save($this->data);

					

				endif;
		

				$i++;

			endforeach;


			$this->Session->setFlash('Initialization Completed');
			$this->redirect(array('action' => 'index'));

	
		else:
			$this->Session->setFlash('You must load a course');
			$this->redirect(array('action' => 'index'));
		endif;
	}














	function index() {
		$this->UserTrainingCourseMap->recursive = 0;
		$this->set('userTrainingCourseMaps', $this->paginate());

			$courses = $this->TrainingCourse->find('all');
			$maps = $this->TrainingCourseMap->find('all');
			$this->set(compact('maps'));
			
			foreach($courses as $c){
				$course_list[$c['TrainingCourse']['id']] = ($c['TrainingCourse']['Code']);
			}
			
			$this->set('courses', $course_list);
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid user training course map', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('userTrainingCourseMap', $this->UserTrainingCourseMap->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->UserTrainingCourseMap->create();
			if ($this->UserTrainingCourseMap->save($this->data)) {
				$this->Session->setFlash(__('The user training course map has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user training course map could not be saved. Please, try again.', true));
			}
		}
		$users = $this->UserTrainingCourseMap->User->find('list');
		$trainingCourseMaps = $this->UserTrainingCourseMap->TrainingCourseMap->find('list');
		$this->set(compact('users', 'trainingCourseMaps'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid user training course map', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->UserTrainingCourseMap->save($this->data)) {
				$this->Session->setFlash(__('The user training course map has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user training course map could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->UserTrainingCourseMap->read(null, $id);
		}
		$users = $this->UserTrainingCourseMap->User->find('list');
		$trainingCourseMaps = $this->UserTrainingCourseMap->TrainingCourseMap->find('list');
		$this->set(compact('users', 'trainingCourseMaps'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for user training course map', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->UserTrainingCourseMap->delete($id)) {
			$this->Session->setFlash(__('User training course map deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('User training course map was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
