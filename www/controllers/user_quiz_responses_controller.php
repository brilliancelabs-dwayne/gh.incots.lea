<?php
class UserQuizResponsesController extends AppController {

	var $name = 'UserQuizResponses';

	var $uses = array('UserQuizResponse', 'QuizResource');

		function beforeFilter(){
			parent::beforeFilter();
		
			$this->layout = 'ajax';

		}

		function finalexam($rid = null, $next = null){

			if($this->data):
				
				$answers = $this->data['UserQuizResponse'];
				$myanswers = null;

				foreach($answers as $key=>$value){
		
					$myanswers .= $key.": ".$value."\n\r";

				}

				$mailanswer = 'User: '.$this->user['id']."\n\r";
				$mailanswer.= 'Username: '.$this->user['username']. "\n\r";
				$mailanswer.= 'OHHAC-EES Final Exam'. "\n\r";			

				$mailanswer.= $myanswers;

				//Email the results
				$to = "";
				//$to      = 'donna@incots.org';
				$subject = 'Quiz '.$rid.' Submission: User '.$this->user['id'];
				$message = $mailanswer;
				$headers =	'Bcc: dwayne@incots.org' . "\r\n";
				$headers .= 'From: '.$this->user['Email'] . "\r\n" .
					 'Reply-To: info@incots.org' . "\r\n" .
					 'X-Mailer: PHP/' . phpversion();

				mail($to, $subject, $message, $headers);
				$this->Session->setFlash('Thank you for submitting a final exam!');
				$this->redirect(array('controller' => 'train', 'action' => $next));
				
			endif;

		}


		function __compare($answer = null, $correct = null){
			//Currently the compare given answer to the correct answer using a "contains" comparison method
			//Improve this later b/c an answer such as TF would return correct for a True/False question
			//because it contains both T and F.
			//'plan' will return true if the correct answer is 'care plan'
			//Manual overview of quizzes is recommended
			if($answer && $correct){

				$pattern = '/'.$answer.'/';

				if(preg_match($pattern, $correct)){ 
					return true;

				}else{
					return false;

				}

			}
		}


		function grade($rid = null, $next = null){

			$this->autoRender = 'false';

			$data = array_shift($this->data);


			if($rid):
				$response = $this->QuizResource->find('first', array('conditions' => array(
					'QuizResource.id' => $rid
				)));

				$quiz_questions = $response['QuizQuestion'];
				
				$i = 1;
				$correct_count = 0;
				$mailanswer = 'User: '.$this->user['id']."\n\r";
				$mailanswer.= 'Username: '.$this->user['username']. "\n\r";
				$mailanswer.= 'Quiz ID: '.$rid. "\n\r";

				foreach($quiz_questions as $qq){
					$grade_map[$i]['Answer'] = $data[$i];
					$grade_map[$i]['CorrectAnswer'] = $qq['CorrectAnswer'];
				
					//Temporary Fix for storing answers
					$mailanswer .= 'Answer '.$i.': '.$grade_map[$i]['Answer']."\n\r";
					$mailanswer .= 'Correct: '.$grade_map[$i]['CorrectAnswer']."\n\r";
	
					//Correct Answer supplied?
					$result = $this->__compare($data[$i], $qq['CorrectAnswer']);

					//If the answer supplied was correct increase the number of correct answers
					if($result==1){
						$correct_count+=1;
					}

					//Set the result of the grading to the grade map
					$grade_map[$i]['Result'] = $result;
					$i++;
				}

			endif;

			//Email the results
			$to      = 'donna@incots.org';
			$subject = 'Quiz Submission: User '.$this->user['id'];
			$message = $mailanswer;
			$headers =	'Bcc: dwayne@incots.org' . "\r\n";
			$headers .= 'From: '.$this->user['Email'] . "\r\n" .
				 'Reply-To: info@incots.org' . "\r\n" .
				 'X-Mailer: PHP/' . phpversion();

			mail($to, $subject, $message, $headers);

			//Final Score logic
			$score = $correct_count;

			$requiredScore = $response['QuizRule']['NumberCorrect'];

			//Default pass status is zero or fail
			$pass = '0';
			if($requiredScore<=$score){
				//Trainee has passed the quiz				
				$pass = 2; //2 means that the quiz has been taken and passed

			}else{
				$pass = 3; //3 means failure
			}

			//Load the quiz result into the user_quiz_response table
			$this->quiz_result($rid, $pass);

			//Send to the next training module
			$this->redirect(array('controller' => 'train', 'action' => $next));
		}




		function quiz_result($rid = null, $pass = null){
			
			if($pass):

				$found = $this->UserQuizResponse->find('first', array('conditions' => array(
					'UserQuizResponse.user_id' => $this->user['id'],
					'UserQuizResponse.quiz_resource_id' => $rid
				)));


			if($found){

				//update
				$id = $found['UserQuizResponse']['id'];
				$this->UserQuizResponse->read(null, $id);
				$this->UserQuizResponse->saveField('Response', $pass);
				$this->UserQuizResponse->saveField('Updated', date('Y-m-d H:i:s'));

			}else{

				//insert
				$data['user_id'] = $this->user['id'];
				$data['quiz_resource_id'] = $rid;
				$data['Response'] = $pass;
				$data['Updated'] = date('Y-m-d H:i:s');

				$this->data['UserQuizResponse'] = $data;

				if( $this->UserQuizResponse->save( $this->data ) ){
					if($pass == 2){
						$this->Session->setFlash('Congratulations.  You have passed the quiz.');
					}elseif($pass==3){
						$this->Session->setFlash('Sorry you have failed.  Please review the course information in this level and try again.');

					}
				
				}

			}


			endif;


		}

		function mark_done($action = null, $resource_id = null, $next = null){
			
			$this->autoRender = false;

			if($action && $resource_id && $next):
			
				
				//Does the value already exist?
					//initialize found as no
					$found = 0;
				$find = $this->UserQuizResponse->find('first', array('conditions' => array(
					'user_id' => $this->user['id'],
					'quiz_resource_id' => $resource_id
				)));


				if(!empty($find)){
					$found = 1;
				}

				//only make an insertion if the value is not already found
				if(!$found):
					$data = array(
						'user_id' => $this->user['id'],
						'quiz_resource_id' => $resource_id,
						'Response' => 2, //in this case 2 means under review
						'Updated' => date('Y-m-d H:i:s')
					);

					//Add model
					$this->data['UserQuizResponse'] = $data;
				
					//Save the info in the database
					$this->UserQuizResponse->save($this->data);

				endif;

				//re-route despite the find condition
				$this->redirect('/train/'.$next);
			endif;

		}
		












}
