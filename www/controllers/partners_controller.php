<?php
class PartnersController extends AppController {

	var $name = 'Partners';
	var $uses = array('Partner', 'User', 'PartnerTrainee');
	var $partners = array();


		function beforeFilter(){

			if(isset($this->Auth)){
				$this->Auth->allow('*');				
				//$this->Auth->allow('activate', 'register', 'login');
			}
		
			$partners = $this->Partner->find('list');
			$pfull = $this->Partner->find('all', array(
				'recursive' => -1
			));
			$this->partners = $partners;

			$this->set(compact('partners'));
		}
		function register(){
	
		}
		function activate(){
			$trainees = $this->PartnerTrainee->find('all', array('conditions' => array(
				
			)));
			//debug($trainees);

			$this->set(compact('trainees'));

		}		
		function download(){
		}
		function proc_register(){
			$this->autoRender = false;
			if($this->data):
				//set up the partner variable then remove this bad boy
				$p['Email'] = 'dwayne.ford+learnquest@brilliancelabs.com';

				$td = $this->data['Partner'];
				if($td['Email']){
					$m = '%s has just enrolled!';
					
					//Send the information to email
					mail($p['Email'], 'New enrollee', sprintf($m,$td['Email']));
					
					$this->redirect('/users/register_add/1/'.$td['Email']);

				}
				

			endif;
		}

		function activate_proc(){
			$this->autoRender = false;
			if($this->data){

				$td = $this->data['Partner'];	
				$td['Code'] = $td['PassCode'];
				$td['PassCode'] = Security::hash($td['PassCode']);

				$find = $this->PartnerTrainee->find('first', array('conditions' => array(
					'PartnerTrainee.id' => $td['Trainee']
				)));

				if($find['PartnerTrainee']['Code'] == $td['Code']){

					debug('Verified');
				}else{

					debug('Failed to Verify');
				}

/*
				if($this->login($this->data)){
					$this->redirect(array(
						'controller' => 'pages',
						'action' => 'display', 'home'
					));
				}
*/
				debug($td);

			}
		}
		function login(){
			$this->autoRender = false;

		}

		function index(){
			$this->redirect = array('action' => 'register');

		}
































/**/


	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid partner', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('partner', $this->Partner->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Partner->create();
			if ($this->Partner->save($this->data)) {
				$this->Session->setFlash(__('The partner has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The partner could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid partner', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Partner->save($this->data)) {
				$this->Session->setFlash(__('The partner has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The partner could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Partner->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for partner', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Partner->delete($id)) {
			$this->Session->setFlash(__('Partner deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Partner was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
