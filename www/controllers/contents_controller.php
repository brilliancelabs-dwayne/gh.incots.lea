<?php 

	class ContentsController extends AppController {
		var $name = 'Contents';
		var $uses = array();


		function beforeFilter(){
			parent::beforeFilter();
			$this->layout = 'ajax';

		}

		function index(){
			//debug('Please choose an action');
		}

		function read($file = null){
			$this->layout = 'ajax';
			$base = "https://docs.google.com/viewer?url=";
			$filebase = "http://www.demaindtraining.org/webroot/OHHAC-EES/read/";
			$file = $filebase.$file."/";
			$url = $base.$file."&embedded=true";
			$readthis = $url;

			$this->set(compact('readthis'));
	
		}	

	}
