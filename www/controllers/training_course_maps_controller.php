<?php
class TrainingCourseMapsController extends AppController {

	var $name = 'TrainingCourseMaps';

	var $paginate = array('limit' => '500');

	function beforeFilter(){
		parent::beforeFilter();
		
	}











	function index() {
		$this->TrainingCourseMap->recursive = 0;
		$this->set('trainingCourseMaps', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid training course map', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('trainingCourseMap', $this->TrainingCourseMap->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->TrainingCourseMap->create();
			if ($this->TrainingCourseMap->save($this->data)) {
				$this->Session->setFlash(__('The training course map has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The training course map could not be saved. Please, try again.', true));
			}
		}
		$trainingCourses = $this->TrainingCourseMap->TrainingCourse->find('list');
		$mapElements = $this->TrainingCourseMap->MapElement->find('list');
		$this->set(compact('trainingCourses', 'mapElements'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid training course map', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->TrainingCourseMap->save($this->data)) {
				$this->Session->setFlash(__('The training course map has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The training course map could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->TrainingCourseMap->read(null, $id);
		}
		$trainingCourses = $this->TrainingCourseMap->TrainingCourse->find('list');
		$mapElements = $this->TrainingCourseMap->MapElement->find('list');
		$this->set(compact('trainingCourses', 'mapElements'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for training course map', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->TrainingCourseMap->delete($id)) {
			$this->Session->setFlash(__('Training course map deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Training course map was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
