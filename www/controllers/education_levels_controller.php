<?php
class EducationLevelsController extends AppController {

	var $name = 'EducationLevels';
	var $scaffold;

	function beforeFilter(){
		parent::beforeFilter();


		$this->Auth->allow('*');
		$this->layout = 'cake.default';
	}

}
