<?php
class UserCaseStudyResponsesController extends AppController {

	var $name = 'UserCaseStudyResponses';




		function beforeFilter(){
			parent::beforeFilter();
			

			$this->layout = 'ajax';

		}	



		function mark_done($action = null, $resource_id = null, $next = null){
			$this->autoRender = false;



			if($action && $resource_id && $next):
			
				
				//Does the value already exist?
					//initialize found as no
					$found = 0;
				$find = $this->UserCaseStudyResponse->find('first', array('conditions' => array(
					'user_id' => $this->user['id'],
					'case_study_resource_id' => $resource_id
				)));


				if(!empty($find)){
					$found = 1;
				}

				//only make an insertion if the value is not already found
				if(!$found):
					$data = array(
						'user_id' => $this->user['id'],
						'case_study_resource_id' => $resource_id,
						'Response' => 2, //in this case 2 means under review
						'Updated' => date('Y-m-d H:i:s')
					);

					//Add model
					$this->data['UserCaseStudyResponse'] = $data;
				
					//Save the info in the database
					$this->UserCaseStudyResponse->save($this->data);

				endif;

				//re-route despite the find condition
				$this->redirect('/train/'.$next);
			endif;

		}
		












}
