<?php
class MapElementsController extends AppController {

	var $name = 'MapElements';

	function index() {
		$this->MapElement->recursive = 0;
		$this->set('mapElements', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid map element', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('mapElement', $this->MapElement->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->MapElement->create();
			if ($this->MapElement->save($this->data)) {
				$this->Session->setFlash(__('The map element has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The map element could not be saved. Please, try again.', true));
			}
		}
		$elementTypes = $this->MapElement->ElementType->find('list');
		$this->set(compact('elementTypes'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid map element', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MapElement->save($this->data)) {
				$this->Session->setFlash(__('The map element has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The map element could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MapElement->read(null, $id);
		}
		$elementTypes = $this->MapElement->ElementType->find('list');
		$this->set(compact('elementTypes'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for map element', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MapElement->delete($id)) {
			$this->Session->setFlash(__('Map element deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Map element was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
