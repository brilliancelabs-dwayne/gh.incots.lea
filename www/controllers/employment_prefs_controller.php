<?php
class EmploymentPrefsController extends AppController {

	var $name = 'EmploymentPrefs';
	var $scaffold;

	function beforeFilter(){
		parent::beforeFilter();

		$this->Auth->allow('*');
		$this->layout = 'cake.default';
	}

}
