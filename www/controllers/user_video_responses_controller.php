<?php

	class UserVideoResponsesController extends AppController {
		
		var $name = 'UserVideoResponses';

		function beforeFilter(){
			parent::beforeFilter();
			

			$this->layout = 'ajax';

		}	


		function mark_done($action = null, $resource_id = null, $next = null){
			$this->autoRender = false;



			if($action && $resource_id && $next):
			
				
				//Does the value already exist?
					//initialize found as no
					$found = 0;
				$find = $this->UserVideoResponse->find('first', array('conditions' => array(
					'user_id' => $this->user['id'],
					'video_resource_id' => $resource_id
				)));


				if(!empty($find)){
					$found = 1;
				}

				//only make an insertion if the value is not already found
				if(!$found):
					$data = array(
						'user_id' => $this->user['id'],
						'video_resource_id' => $resource_id,
						'Response' => 1, //in this case 1 just means done or completed
						'Updated' => date('Y-m-d H:i:s')
					);

					//Add model
					$this->data['UserVideoResponse'] = $data;
				
					//Save the info in the database
					$this->UserVideoResponse->save($this->data);

				endif;

				//re-route despite the find condition
				$this->redirect('/train/'.$next);
			endif;

		}
		
	}
