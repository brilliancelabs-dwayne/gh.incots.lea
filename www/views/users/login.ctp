<?php echo $this->Html->css('login');?>
<script>
$(document).ready(function(){
	var size = screen.width+'px '+1000+'px';
	$('#content').css({
		'height': screen.height,
		'background-size': size
	});
});

</script>	

<div class="container">
	<div class="page">
		<div class="login_box">
			<table class="login shadow_overhead">
			<tr class="window_bar">
				<th>&nbsp;</th>
				<th><span>x</span></th>
			</tr>
			<tr>
				<td class="notebox">
				<?php echo $this->Form->create('User', array('action' => 'login'));?>
					<table class="nb_content">
						<tr class="title">
						<th>Sign-in to your account</th>
						</tr>
						<tr>
							<td>
							<table class="credentials">
							<tr><td class="label">Username:</td>
							<td>&nbsp;</td>
							<td><?php echo $this->Form->input('username', array('label'=>'','class' => 'rounded'));?>
							</td>
							</tr>

							<tr><td class="label">Password:</td>
							<td>&nbsp;</td>
							<td><?php echo $this->Form->input('password', array('label'=>'','class' => 'rounded'));?></td>
							</tr>

							<tr>
								<td colspan="3">
									<table class="pass_reminder">
										<tr>			
											<td><?php echo $this->Html->link('Forgot your password?','/users/password-reset', array(
														'class' => 'forgot'
													));?>
											</td>
											<td class="right">
												<?php echo $this->Form->submit('Sign-in', array('class' => 'rounded enhanced_btn'));?>
											</td>
										</tr>
									</table>
								</td>
							</tr>

							</table>
							</td>
					</table>
				<?php echo $this->Form->end();?>
				</td>
		<!--Right Pane-->
				<td class="notebox">
				<table class="nb_content">
					<tr class="title">
					<th>Register for an account</th>
					</tr>
					<tr>
						<td>To train in this system you will need to register with one of our non-profit partner organizations.  Please click the button below to proceed.</td>
					</tr>
					<tr><td>&nbsp;</td>
					</tr>
					<tr>
						<td><input type="submit" rel="/register" value="Begin Registration" name="activate" class="btn_alt rounded" />
						</td>
					</tr>
				</table>
				</td>
			</tr>
			</table>
		</div>
	</div>
</div>
