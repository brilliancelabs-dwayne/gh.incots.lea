<?php echo $this->element('/aj_styles');?>
<?php echo $this->Html->script('jquery-latest');?>
</script>
<script>
$(document).ready(function(){
	$('form').submit(function(e){
		
		e.preventDefault();
		
		var pw = $('input:password').val();
		var pws = $('#pws').val();

		if(pw!="" && pws == ""){
			
			$('#pws').val(pw);
			//alert('pw was set');

			$('input:password').val('');
			$('#confirm_pass').val('Confirm');
		
		}else if(pw!="" && pws !=""){
			if(pw == pws){
				//alert('running');
				$('form').submit();
			}else{
			$('#pws').val('');
			$('input:password').val('');
			$('#confirm_pass').val('Save');
			$('.emessage').html('Your passwords did not match.  Please try again.');
			$('.emessage').css({
				'color': 'red',
				'position': 'absolute',
				'top': 0,
				'text-align': 'center'
			});

			
			}
		}else{
			//alert('pw: '+pw+' pws: '+pws);
		}

	})
})
</script>
<h2 class="page_header">Complete Your Registration</h2>
<?php echo $this->Form->create('User', array('controller' => 'users', 'action' => 'add'));?>
<fieldset><legend>Set Your Password</legend>
<div class="emessage"></div>
<?php echo $this->Form->input('Email', array('disabled' => 'disabled',
	'value' => $this->params['pass'][1]
));?>

<?php echo $this->Form->input('Password', array('type' => 'password'));?>
<?php echo $this->Form->input('Stor', array('id' => 'pws', 'value' => ''));?>
<input type="submit" id="confirm_pass" value="Save" />



</fieldset>
<?php echo $this->Form->end();?>
