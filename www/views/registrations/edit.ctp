<div class="registrations form">
<?php echo $this->Form->create('Registration');?>
	<fieldset>
		<legend><?php __('Edit Registration'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('partner_id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('Active');
		echo $this->Form->input('BirthDate');
		echo $this->Form->input('Gender');
		echo $this->Form->input('Ethnicity');
		echo $this->Form->input('Street1');
		echo $this->Form->input('Street2');
		echo $this->Form->input('City');
		echo $this->Form->input('State');
		echo $this->Form->input('Zip');
		echo $this->Form->input('PrimaryPhone');
		echo $this->Form->input('MobilePhone');
		echo $this->Form->input('Unemployed');
		echo $this->Form->input('LastDateEmployed');
		echo $this->Form->input('employment_pref_id');
		echo $this->Form->input('education_level_id');
		echo $this->Form->input('FirstCourse');
		echo $this->Form->input('OfficeEnrollmentDate');
		echo $this->Form->input('OfficeCourseCompletionDate');
		echo $this->Form->input('OfficeHireDate');
		echo $this->Form->input('OfficeHiringFirm');
		echo $this->Form->input('OfficeLeadSource');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Registration.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Registration.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Registrations', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Partners', true), array('controller' => 'partners', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Partner', true), array('controller' => 'partners', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employment Prefs', true), array('controller' => 'employment_prefs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employment Pref', true), array('controller' => 'employment_prefs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Education Levels', true), array('controller' => 'education_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Education Level', true), array('controller' => 'education_levels', 'action' => 'add')); ?> </li>
	</ul>
</div>