<div class="registrations index">
	<h2><?php __('Registrations');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('partner_id');?></th>
			<th><?php echo $this->Paginator->sort('user_id');?></th>
			<th><?php echo $this->Paginator->sort('Active');?></th>
			<th><?php echo $this->Paginator->sort('BirthDate');?></th>
			<th><?php echo $this->Paginator->sort('Gender');?></th>
			<th><?php echo $this->Paginator->sort('Ethnicity');?></th>
			<th><?php echo $this->Paginator->sort('Street1');?></th>
			<th><?php echo $this->Paginator->sort('Street2');?></th>
			<th><?php echo $this->Paginator->sort('City');?></th>
			<th><?php echo $this->Paginator->sort('State');?></th>
			<th><?php echo $this->Paginator->sort('Zip');?></th>
			<th><?php echo $this->Paginator->sort('PrimaryPhone');?></th>
			<th><?php echo $this->Paginator->sort('MobilePhone');?></th>
			<th><?php echo $this->Paginator->sort('Unemployed');?></th>
			<th><?php echo $this->Paginator->sort('LastDateEmployed');?></th>
			<th><?php echo $this->Paginator->sort('employment_pref_id');?></th>
			<th><?php echo $this->Paginator->sort('education_level_id');?></th>
			<th><?php echo $this->Paginator->sort('FirstCourse');?></th>
			<th><?php echo $this->Paginator->sort('OfficeEnrollmentDate');?></th>
			<th><?php echo $this->Paginator->sort('OfficeCourseCompletionDate');?></th>
			<th><?php echo $this->Paginator->sort('OfficeHireDate');?></th>
			<th><?php echo $this->Paginator->sort('OfficeHiringFirm');?></th>
			<th><?php echo $this->Paginator->sort('OfficeLeadSource');?></th>
			<th><?php echo $this->Paginator->sort('Created');?></th>
			<th><?php echo $this->Paginator->sort('Updated');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($registrations as $registration):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $registration['Registration']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($registration['Partner']['Name'], array('controller' => 'partners', 'action' => 'view', $registration['Partner']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($registration['User']['Email'], array('controller' => 'users', 'action' => 'view', $registration['User']['id'])); ?>
		</td>
		<td><?php echo $registration['Registration']['Active']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['BirthDate']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['Gender']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['Ethnicity']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['Street1']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['Street2']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['City']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['State']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['Zip']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['PrimaryPhone']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['MobilePhone']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['Unemployed']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['LastDateEmployed']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($registration['EmploymentPref']['id'], array('controller' => 'employment_prefs', 'action' => 'view', $registration['EmploymentPref']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($registration['EducationLevel']['id'], array('controller' => 'education_levels', 'action' => 'view', $registration['EducationLevel']['id'])); ?>
		</td>
		<td><?php echo $registration['Registration']['FirstCourse']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['OfficeEnrollmentDate']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['OfficeCourseCompletionDate']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['OfficeHireDate']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['OfficeHiringFirm']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['OfficeLeadSource']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['Created']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['Updated']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $registration['Registration']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $registration['Registration']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $registration['Registration']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $registration['Registration']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Registration', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Partners', true), array('controller' => 'partners', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Partner', true), array('controller' => 'partners', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employment Prefs', true), array('controller' => 'employment_prefs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employment Pref', true), array('controller' => 'employment_prefs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Education Levels', true), array('controller' => 'education_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Education Level', true), array('controller' => 'education_levels', 'action' => 'add')); ?> </li>
	</ul>
</div>