<style type="text/css">
<!--
h2 {
	font-size: 36px;
	font-weight: bold;
	letter-spacing: 2px;

}
fieldset {
	width: 75%;
	padding: 15px;
	background: rgba(0,0,0,0.75);
	color: #FFF;
	-webkit-border-radius: 15px;	
	-moz-border-radius: 15px;
	border-radius: 15px;
}
form div {
	padding: 15px 15px 15px 35px;
}

form div label {
	float: left;
	width: 150px;	
	height: 30px;
	font-size: 16px;
	letter-spacing: 1px;
	padding: 10px 0px 0px 0px;
}
input,password,select{
	border: solid thin white;
	padding: 7px;

}
.page {
	padding: 25px;
}
input[type=submit] {
	padding: 15px 45px;
	-webkit-border-radius: 10px;	
	-moz-border-radius: 10px;
	border-radius: 10px;
}
input[type=submit]:hover{
	background: #CCC;
	cursor: pointer;
}
-->
</style>
<h2>Training Registration</h2>
<div class="page">
<div class="registrations form">
<?php echo $this->Form->create('Registration');?>
	<fieldset>
	<?php
		echo $this->Form->input('partner_id');
		echo $this->Form->hidden('user_id', array(
			'value' => '0'
		));
		echo $this->Form->hidden('Active', array(
			'value' => '1'
		));
		echo $this->Form->input('BirthDate');
		echo $this->Form->input('Gender', array(
			'type' => 'select',
			'options' => array(
				'F'=>'Female',
				'M'=>'Male'	
			)
		));
		echo $this->Form->input('Ethnicity');
		echo $this->Form->input('Street1');
		echo $this->Form->input('Street2');
		echo $this->Form->input('City');
		echo $this->Form->input('State', array(
			'size' => '2',
			'value' => 'OH'
		));
		echo $this->Form->input('Zip');
		echo $this->Form->input('PrimaryPhone');
		echo $this->Form->input('MobilePhone');
		echo $this->Form->input('Unemployed');
		echo $this->Form->input('LastDateEmployed');
		echo $this->Form->input('employment_pref_id');
		echo $this->Form->input('education_level_id');
		echo $this->Form->input('FirstCourse', array(
			'type' => 'select',
			'options' => $first_courses
		));
		echo $this->Form->input('OfficeEnrollmentDate');
		echo $this->Form->input('OfficeCourseCompletionDate');
		echo $this->Form->input('OfficeHireDate');
		echo $this->Form->input('OfficeHiringFirm');
		echo $this->Form->input('OfficeLeadSource');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Register', true));?>
</div>
</div>
