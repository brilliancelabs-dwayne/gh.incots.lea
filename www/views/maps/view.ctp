<?php $this->layout = 'ajax';?>
<style>
<!--
h2 {
	font-size: 1.5em;
}

.course_map .course_action {
	cursor: pointer;
}

.course_map th {
	padding: 15px;
	background: lightblue;
	text-align: left;
	font-weight: bold;
	border-bottom: solid 2px #333;
	border-top: solid 2px #333;
}

.course_map td {
	padding: 20px;
	background: #777;
	color: white;
	text-align: left;
}

.course_map td:first-child {
	font-size: 1.3em;
	color: white;
	font-weight: bold;

}

.course_map tr.day_1 td {
	background: #777;
}
.course_map tr.day_2 td {
	background: #777;
}
.course_map tr.day_3 td {
	background: #777;
}
.course_map tr.day_4 td {
	background: #777;
}
.course_map tr.day_5 td {
	background: #777;
}

.course_map tr.day_6 td {
	background: #444;
}
.course_map tr.day_7 td {
	background: #444;
}
.course_map tr.day_8 td {
	background: #444;
}
.course_map tr.day_9 td {
	background: #444;
}
.course_map tr.day_10 td {
	background: #444;
}

.course_map tr.day_11 td {
	background: #222;
}
.course_map tr.day_12 td {
	background: #222;
}
.course_map tr.day_13 td {
	background: #222;
}

-->
</style>
<h2>Training Course Outline</h2>
<h3>Course: OHHAC-EES</h3>


<table class = "course_map">
<tr>
	<th>Day</th>
	<th>&nbsp;</th>
	<th>#</th>
	<th>Lesson</th>
	<th>Status</th>
</tr>

<?php
	if(isset($courses) && is_array($courses)){
		foreach($courses as $c):
			$cm = $c['training_course_maps'];
?>
<tr class="day_<?php echo $cm['Day']?>">
	<td><?php echo "Day ".$cm['Day']?></td>
	<td class="group_<?php echo $cm['Grouping']?>">&nbsp;</td>
	<td><?php echo $cm['#']?></td>
	<td><?php echo $cm['Lesson']?></td>
	<td class="course_action">
	<?php
	/*
	<a href="/train/<?php echo $cm['#'];?>">
		<img src="/img/check.png" width="25" />
	</a>
	*/
	?>
	</td>
</tr>


<?php
		endforeach;
	}
?>

</table>
