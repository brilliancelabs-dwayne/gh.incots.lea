<?php 

if(isset($_SERVER['HTTP_HOST'])){
	
	$host = $_SERVER['HTTP_HOST'];

}else{
	
	$host = 'localhost';

}

?>


<?php 
/**Level Access Logic**/
$la = null;
if(isset($levelAccess) && isset($tcm_current)){
	$la = $levelAccess;
	$req = $tcm_current;
}

if(!$la):
?>
<h2>Sorry there has been a problem.</h2>
<h3>Please request access to this course</h3>

<?php
elseif($la=='deny'):
?>
<h2>Sorry, you do not have access to this course element</h2>
<h3 class="button">
<?php 
/**Send to training course map if illegal element is loaded
**/
echo $this->Html->link('Go here instead', array(
	'controller' => 'train',
	'action' => 1
));
?>
</h3>
<?php
else:
/**Load the view if approved**/
?>

<?php 
/***
** Start the course
**
**/

echo $this->element('aj_styles');?>

<div class="element_bar">
	<span class="course_name"><?php echo $courses['CourseName'];?></span>
	<span class="course_code"><?php echo $courses['Code'];?></span>
</div>

<div class="nav_bar">
	<span class="nav_left">
		<?php echo $this->Html->link('<', array(
			'action' => 'index', $tcm_prev	
		), array('class' => 'nav_left'));
		?>
	</span>

	<span>
		<?php echo $this->Html->link('>', array(
			'action' => 'index', $tcm_next	
		), array('class' => 'nav_right'));
		?>
	</span>

<?php
/**Render the training course Map**/
?>
<?php 
	//Turn map order number into array number
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

?>

	<span class="element_counter"><?php echo $tcm_current;?>/<?php echo $tcm_total;?></span>
	<span class="element_name"><?php echo $element_name;?></span>

</div>
<div class="show_me">
	<?php echo $this->element('render');?>
</div>

<?php 
/**End**/
endif;
?>
