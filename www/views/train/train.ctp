<?php 
/***
** Start the course
**
**/

echo $this->element('aj_styles');?>

<div class="element_bar">
	<span class="course_name"><?php echo $course_name;?></span>
	<span class="course_code"><?php echo $course_code;?></span>
</div>

<div class="nav_bar">


<?php
/**Render the training course Map**/
?>
	<span class="element_counter"><?php echo $element;?>/<?php echo $course_elements;?></span>
	<span class="element_name"><?php echo $element_name;?></span>

</div>
<div class="show_me">
	<?php echo $this->element('rendering');?>
</div>
