<?php
class MediaHelper extends AppHelper {
	public $helpers = array("Html");
   
	public function youtube($code = null, $w = 560, $h = 315){
      /**Credit for z-index issue fix:
       * http://stackoverflow.com/questions/9074365/youtube-video-embedded-via-iframe-ignoring-z-index
       */
		$codeblock = '
		<iframe 
			width="%d" height="%d" 
			src="https://www.youtube-nocookie.com/embed/%s?rel=0&wmode=transparent" 
			frameborder="0" allowfullscreen wmode="Opaque">
		</iframe>';
		
		if($code&&$w&&$h):
			$video = sprintf($codeblock, $w, $h, $code);
			return $video;
		endif;
		
	
	}
   
   public function app($id = null){
      $load = ClassRegistry::init('Application', 'Model');
      $app = new $load;
      $icon = $app->find('first');
      
      //Seems to work but needs further development 
      //echo $this->Html->image($icon['application']['Icon']);
   }
}
?>
