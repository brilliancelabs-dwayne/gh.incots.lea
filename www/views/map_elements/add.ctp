<div class="mapElements form">
<?php echo $this->Form->create('MapElement');?>
	<fieldset>
		<legend><?php __('Add Map Element'); ?></legend>
	<?php
		echo $this->Form->input('element_type_id');
		echo $this->Form->input('Name');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Map Elements', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Element Types', true), array('controller' => 'element_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Element Type', true), array('controller' => 'element_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Training Course Maps', true), array('controller' => 'training_course_maps', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Training Course Map', true), array('controller' => 'training_course_maps', 'action' => 'add')); ?> </li>
	</ul>
</div>