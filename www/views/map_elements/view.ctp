<div class="mapElements view">
<h2><?php  __('Map Element');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $mapElement['MapElement']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Element Type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($mapElement['ElementType']['id'], array('controller' => 'element_types', 'action' => 'view', $mapElement['ElementType']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $mapElement['MapElement']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $mapElement['MapElement']['Updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Map Element', true), array('action' => 'edit', $mapElement['MapElement']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Map Element', true), array('action' => 'delete', $mapElement['MapElement']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $mapElement['MapElement']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Map Elements', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Map Element', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Element Types', true), array('controller' => 'element_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Element Type', true), array('controller' => 'element_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Training Course Maps', true), array('controller' => 'training_course_maps', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Training Course Map', true), array('controller' => 'training_course_maps', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Training Course Maps');?></h3>
	<?php if (!empty($mapElement['TrainingCourseMap'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Training Course Id'); ?></th>
		<th><?php __('MapOrder'); ?></th>
		<th><?php __('MapLevel'); ?></th>
		<th><?php __('Training Course Module'); ?></th>
		<th><?php __('MapElementName'); ?></th>
		<th><?php __('Map Element Id'); ?></th>
		<th><?php __('Resource'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($mapElement['TrainingCourseMap'] as $trainingCourseMap):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $trainingCourseMap['id'];?></td>
			<td><?php echo $trainingCourseMap['training_course_id'];?></td>
			<td><?php echo $trainingCourseMap['MapOrder'];?></td>
			<td><?php echo $trainingCourseMap['MapLevel'];?></td>
			<td><?php echo $trainingCourseMap['training_course_module'];?></td>
			<td><?php echo $trainingCourseMap['MapElementName'];?></td>
			<td><?php echo $trainingCourseMap['map_element_id'];?></td>
			<td><?php echo $trainingCourseMap['resource'];?></td>
			<td><?php echo $trainingCourseMap['Updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'training_course_maps', 'action' => 'view', $trainingCourseMap['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'training_course_maps', 'action' => 'edit', $trainingCourseMap['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'training_course_maps', 'action' => 'delete', $trainingCourseMap['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $trainingCourseMap['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Training Course Map', true), array('controller' => 'training_course_maps', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
