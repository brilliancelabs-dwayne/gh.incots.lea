<div class="mapElements index">
	<h2><?php __('Map Elements');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('element_type_id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('Updated');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($mapElements as $mapElement):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $mapElement['MapElement']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($mapElement['ElementType']['id'], array('controller' => 'element_types', 'action' => 'view', $mapElement['ElementType']['id'])); ?>
		</td>
		<td><?php echo $mapElement['MapElement']['Name']; ?>&nbsp;</td>
		<td><?php echo $mapElement['MapElement']['Updated']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $mapElement['MapElement']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $mapElement['MapElement']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $mapElement['MapElement']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $mapElement['MapElement']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Map Element', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Element Types', true), array('controller' => 'element_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Element Type', true), array('controller' => 'element_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Training Course Maps', true), array('controller' => 'training_course_maps', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Training Course Map', true), array('controller' => 'training_course_maps', 'action' => 'add')); ?> </li>
	</ul>
</div>