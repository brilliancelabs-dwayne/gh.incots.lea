<?php
	###IMPORTANT
	//This fix will create a variable which can be checked before loaded content-specific layout structures
	$isModule = true;
	//module refers to whether or not a training module has been loaded

	if(isset($this->params['pass'][0])):

		$action = $this->params['pass'][0];

		if(!is_numeric($action)):
			$isModule = false;

		endif;

	endif;
?>
<style>
<!--
/*Agreements*/
div.para {
	padding: 25px;
}
div.para h2 {
	font-size: 1.75em;
}
div.para h3 {
	padding: 15px 5px 5px;
	font-size: 1.25em;
	letter-spacing: -2px;
	font-weight: bold;
	color: yellowgreen;
}

div.para li h4 {
	font-weight: bold;
	font-size: 1.15em;	
}
h5 {
	font-weight: bold;
	color: #777;
}
div.para p {
	padding: 5px;
	max-width: 750px;
}

ul.main li {
	padding: 5px 15px;
	list-style-type: none;
}

div.para ul.sub li {
	padding: 5px 15px;
	list-style: disc inside none;
}
-->
</style>
<?php
/**Has this element been completed**/
?>
<?php

	//Set the action associated with the map element types
	switch($map_element_id):
	
		case 1:
		$type = 'Agreement';
		$action = 'agreeing';
		$mad_msg = "I agree";
		$controller = "user_agreement_responses";
		$model = 'UserAgreementResponse';		
		$ridn = 'agreement_resource_id'; //resource id name
		break;

		case 2:
		$type = 'Notice';
		$action = 'noticing';
		$mad_msg = "I understand";
		$controller = "user_notice_responses";
		$model = 'UserNoticeResponse';
		$ridn = 'notice_resource_id';
		break;

		case 3:
		$type = 'Reading';
		$action = 'reading';
		$mad_msg = "I'm done reading";
		$controller = "user_reading_responses";
		$model = 'UserReadingResponse';
		$ridn = 'reading_resource_id';
		break;

		case 4:
		$type = 'Case Study';
		$action = 'considering';
		$mad_msg = "I'm done";
		$controller = "user_case_study_responses";
		$model = 'UserCaseStudyAnswer';
		$ridn = 'case_study_resource_id';
		break;

		case 5:
		$type = 'Quiz';
		$action = 'taking';
		$mad_msg = "Submit Quiz";
		$controller = "user_quiz_responses";
		$model = 'UserQuizResponse';
		$ridn = 'quiz_resource_id';
		break;

		case 6:
		$type = 'Video';
		$action = 'watching';
		$mad_msg = "I've watched";
		$controller = "user_video_responses";
		$model = 'UserVideoResponse';
		$ridn = 'video_resource_id';
		break;

		default:
		$type = 'error';
		$action = 'throwing';
		$mad_msg = "Report Error to IT";
		$controller = "train";
		$model = null;
		$ridn = null;
	
	endswitch;


?>
	<?php
	//Build the Mark as Done link
	$msg = 'Are you sure you want to submit this training element as "COMPLETED"?';
	
	$MaD = $this->Html->link($mad_msg, array('controller' => $controller, 'action' => 'mark_done', $map_element_id, $rid, $next), array(
		'class' => 'mark_done'
	), $msg);

	?>

	<?php 
	//Hack
	$element_completed = null;
	if(!$element_completed):?>
	<span class="done">
		<?php echo $MaD;?>
	</span>
	<?php endif;?>

<?php 
if(isset($isModule)):
	echo $this->element($action);
endif;
?>
