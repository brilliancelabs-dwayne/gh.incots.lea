<style>
<!--
/*Case Study Styles*/
ul li {
	list-style: disc inside;
	text-indent: 15px;
	margin: 5px;
}
.sub {
	font-weight: bold;
	font-family: Arial, sans-serif;
	padding: 10px;
	margin-top: 15px;
	margin-left: 15px;
	
}
ul {
	padding: 15px 15px;
}
textarea {
	width: 50%;
	max-width: 750px;
	height: 250px;
	padding: 5px;
	margin-left: 35px;
}
div.submit {
	background: royalblue;
	padding: 5px 15px;
	margin: 25px 35px;
	width: 20%;
}
div.submit input[type=submit] {
	padding: 15px;
	width: 100%;
}
div.submit input:hover {
	color: royalblue;
}
-->
</style>
<h2 class="page_header">Consider</h2>
<?php
//Build the link to the static agreement page
//the number 4 is hardcoded b/c that is the element type ID for case studies
$case = $course_id.'/4/'.$rid;
?>
<div class="para">
<?php echo $this->element($case);?>
</div>
