<?php echo $this->element('aj_styles');?>
<style>
<!--
.para {
	padding: 0px 45px;
	color: green;
	text-align: justify;
}
h3 {
	font-weight: bold;
	padding: 15px 15px 5px 0px;
}
ul {
	padding: 15px;
}
li {
	list-style: disc inside;
	margin: 5px;
}
-->
</style>
<h2 class="page_header">Welcome to the Home Health Aide Training Course</h2>
<div class="para">
Welcome students to Incots Online Training/Certification Program for Home Health Aides. It is our pleasure to
have you enrolled in our online pilot program. We are excited to offer a complete, efficient method to training
and certification as a Home Health Aide. Our program complies with all Federal, State and Local Regulations for
training Home Health Aides and we have established the most convenient and efficient methods of obtaining your
certification.

<h3>In order to obtain certification, the following requirements must be fulfilled:</h3>
<ul>
<li>Students must provide a brief introduction of themselves to the online instructor before starting the
program, via email.</li>
<li>Students are required to complete the online Orientation Review.</li>
<li>Students will be required to complete all coursework listed on the Incots Course site.</li>
<li>Students must sign off at the end of each module</li>
<li>Obtain 75% on all chapter quizzes (max. of 2 attempts) and 75% on the final exam (max. of 1 attempt).</li>
<li>Attend 2 laboratory skills classes, each at 8.5 hours a day; your performance in skills is based on a pass/fail
basis.</li>
<li>All coursework is to be done chronologically; you are not allowed to skip ahead and/or complete more
than one module (day of coursework) on any given day.</li>
<li>Laboratory components will not be scheduled until the student has completed all online requirements,
with the exception of the final exam, which is administered after the laboratory component and online
coursework.</li>
</ul>

<h3>Please refer to the orientation review for more information on lab scheduling
timelines.</h3>

Your Orientation Review highlights the specifics of the Incots’ online HHA Certification Program and we welcome
any comments or concerns you may have after completion of this review and subsequent coursework. Please
allow 24 hours for responses from your Instructor.

Exception: If you don’t understand a concept from the book or have a question that divvents you from moving
forward with the modules, please send an email to your instructor. These emails should contain Top Priority as
part of the subject title and will be answered as soon as possible.

Additional suggestions:

Take your time with the reading assignments. If the content seems challenging, read the chapter twice.
Remember there is no rush to complete the course in 2 weeks; this is a self-paced program (see program
requirements for time limits).
Take notes when reviewing the videos. The online videos give you the opportunity to know what you
will be tested on during the skills lab component. Sometimes the steps to a skill must be learned in the
order it is taught, so get in the habit of learning each step. You are not required to memorize the verbal
instructions however.
Do not overlook submitting the case studies. Although there is no assigned grade for submission, it is a
coursework requirement, and you will only receive certification if you complete each case study, as well
as all other requirements.
If you don’t understand something, contact your instructor. You will receive an email to all questions you
have.
Be sure to order the correct size for your scrubs. Any necessary returns will be at your expense and a
shipping/handling fee will be assessed.
</div>
