<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>
<h2>Case Study</h2>
Your patient, Mrs. Parker, is wheelchair bound and is incontinent of urine and stool on occasion. She has advanced Parkinson’s disease and is not able to tell you when she has to have a bowel movement. Today she has had three accidents and you have run out of gloves. You cross your fingers that she is done for today, because you would have no way to clean her and her area. You make it for the next hour and it is time to leave. You hear the key in the door and it is her daughter returning from work. You bend down to pick up your work bag and you notice a brown stain on Mrs. Parker’s robe and that all too familiar smell of stool.<p class="sub">Discuss the following:</p>
<p class="sub">Discuss the following:</p>
<ul>
	<li>Do you get your bag, say good bye and leave; pretending that you didn’t notice it?</li>
	<li>Do you let the daughter know that it just happened and that your shift is over?</li>
	<li>Do you offer to help clean the patient and go to the store for more gloves?</li>
</ul>

<?php echo $this->Form->create('UserCaseStudyAnswer', array('action' => 'mark_done'));?>
<?php echo $this->Form->hidden('case_study_resource_id', array('value' => $rid));?>
<?php echo $this->Form->hidden('Next', array('value' => $tcm_next));?>
<?php echo $this->Form->textarea('Answer', array('label' => 'Your Response'));?>
<?php echo $this->Form->submit('Save My Answer');?>
<?php echo $this->Form->end();?>
