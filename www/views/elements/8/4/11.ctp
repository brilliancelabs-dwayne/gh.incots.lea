<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>
<h2>Case Study</h2>
Your patient is Mrs. Callaway, a 86 year old with dementia. Most times she is pleasant and easy to care for but for the last few weeks it has been difficult to do personal care services for you. She is very combative today and is refusing a shower, although you know it has been at least 4 days since her last one. She has strong body odor and is scheduled to see her doctor today.<ul>
	<li>How do you convince Mrs. Callaway to take a shower?</li>
	<li>Do you call her son and warn him that he may need to reschedule the appointment if she refuses the shower?</li>
	<li>Do you let her go to the doctor without one, and let it be their problem at the doctor’s office?</li>
</ul>

<?php echo $this->Form->create('UserCaseStudyAnswer', array('action' => 'mark_done'));?>
<?php echo $this->Form->hidden('case_study_resource_id', array('value' => $rid));?>
<?php echo $this->Form->hidden('Next', array('value' => $tcm_next));?>
<?php echo $this->Form->textarea('Answer', array('label' => 'Your Response'));?>
<?php echo $this->Form->submit('Save My Answer');?>
<?php echo $this->Form->end();?>
