<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>
<h2>Case Study</h2>
You have been instructed on many occasions that applying heat to a patient must be done by a nurse, that there must be a doctor’s order for it as well. Your patient, Mr. Jackson, has regular back spasms that can be quite severe. When you report to work today, there is a note from his wife, explaining how bad the night was for them because of Mr. Jackson’s back spasms. Mrs. Jackson instructs you to continue applying the heating pad on his lower back, every hour. She explains that the pain medication is not helping and this is the only way Mr. Jackson is not in severe pain.
<p class="sub">Discuss the following:</p>
<ul>
	<li>Do you continue with the heating pad so that your patient won’t be in severe pain?</li>
	<li>Do you call Mrs. Jackson at work and explain why you can’t use the heating pad for her husband, knowing she is mean and confrontational?</li>
	<li>Do you suggest to Mr. Jackson that he try the pain medication again?</li>
	<li>Is this an issue that you would call your supervisor about and/or document on your paperwork?</li>
</ul>

<?php //debug($map);
$next = $map['MapOrder']+1;
?>
<?php echo $this->Form->create('UserCaseStudyAnswer', array('action' => 'mark_done'));?>
<?php echo $this->Form->hidden('case_study_resource_id', array('value' => $rid));?>
<?php echo $this->Form->hidden('Next', array('value' => $tcm_next));?>
<?php echo $this->Form->textarea('Answer', array('label' => 'Your Response'));?>
<?php echo $this->Form->submit('Save My Answer');?>
<?php echo $this->Form->end();?>
