<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>
<h2>Case Study</h2>
Your patient has high blood pressure and it is becoming worse. One reason is that he continues to put a lot of salt on his food and he eats Lays potato chips every day. You know that his wife also cooks with a lot of salt and she refuses to change, saying that her job is to prepare a good meal for her husband.
<p class="sub">Discuss the following:</p>
<ul>
	<li>Do you remind the patient and his wife about the care plan that instructs him to reduce salt intake?
	<li>Do you buy some Mrs. Dash, a salt substitute, for them and suggest they try it?
	<li>Do you report it to the dietitian who is scheduled for a visit later that day?
	<li>Is this an issue that you would call your supervisor about and/or document on your paperwork?
</ul>

<?php echo $this->Form->create('UserCaseStudyAnswer', array('action' => 'mark_done'));?>
<?php echo $this->Form->hidden('case_study_resource_id', array('value' => $rid));?>
<?php echo $this->Form->hidden('Next', array('value' => $tcm_next));?>
<?php echo $this->Form->textarea('Answer', array('label' => 'Your Response'));?>
<?php echo $this->Form->submit('Save My Answer');?>
<?php echo $this->Form->end();?>
