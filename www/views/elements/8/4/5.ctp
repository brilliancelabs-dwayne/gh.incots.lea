<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>
<h2>Case Study</h2>
You have a new assignment this week. You are taking care of an elderly man who lives with his two daughters. He is recovering from hip replacement surgery and the care plan instructed you to assist the patient with ambulation twice a day. The first week goes smoothly but when you report for work on Monday of the second week, the oldest daughter, Emily tells you that her dad has had a rough weekend and not to walk him. You are a bit uncertain about what to do, but the physical therapist visits that morning and states that the best thing for Mr. Thomas, is to continue with ambulation. Emily has gone out and does not hear this. You tell her what the PT has said and she is firm about her dad not being walked.
<p class="sub">Discuss the following:</p>
<ul>
<li>Do you wait until Emily leaves for work and then assist Mr. Thomas with ambulation?</li>
<li>Do you listen to her and disregard the care plan instructions?</li>
<li>Do you ask Mr. Thomas what he wants to do?</li>
</ul>

<?php echo $this->Form->create('UserCaseStudyAnswer', array('action' => 'mark_done'));?>
<?php echo $this->Form->hidden('case_study_resource_id', array('value' => $rid));?>
<?php echo $this->Form->hidden('Next', array('value' => $tcm_next));?>
<?php echo $this->Form->textarea('Answer', array('label' => 'Your Response'));?>
<?php echo $this->Form->submit('Save My Answer');?>
<?php echo $this->Form->end();?>
