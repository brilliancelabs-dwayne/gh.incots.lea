<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>
<h2>Case Study</h2>
Every Wednesday, you go to the grocery store for your patient. Today Mrs. Washington has $14.00 change back from her purchases. You attempt to give it to her but she tells you to put it in her jewelry box on her dresser. You count it out again, $14.00 even; and then you put it in the jewelry box and go home. The next day Mrs. Washington tells you that the money is not in her jewelry box.
<p class="sub">Discuss the following:</p>
<ul>
	<li>You know that her son was there yesterday and could have taken it for his drug habit; do you tell her this?</li>
	<li>Do you offer to replace the money?</li>
	<li>Do you tell her that it’s her fault because she wouldn’t let you put the change in her hand?</li>
</ul>



<?php //debug($map);
$next = $map['MapOrder']+1;
?>
<?php echo $this->Form->create('UserCaseStudyAnswer', array('action' => 'mark_done'));?>
<?php echo $this->Form->hidden('case_study_resource_id', array('value' => $rid));?>
<?php echo $this->Form->hidden('Next', array('value' => $next));?>
<?php echo $this->Form->textarea('Answer', array('label' => 'Your Response'));?>
<?php echo $this->Form->submit('Save My Answer');?>
<?php echo $this->Form->end();?>
