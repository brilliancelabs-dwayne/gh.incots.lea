<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>
<h2>Elder Abuse</h2>
<p>You are about to leave Mr. Smith's home after working 6 hours. You are saying goodbye to him when you hear his adult son and daughter arguing in the kitchen. Mr. Smith looks fearful and begs you to stay longer.</p>
<p class="sub">Discuss the following:</p>
<ul>
	<li>If you leave, what responsibility do you have for the patient's safety?</li>
	<li>Is it appropriate to stay with the patient until the argument is over?</li>
	<li>Is this an issue that you would call your supervisor about and/or document on your paperwork?</li>
</ul>
<?php //debug($map);
$next = $map['MapOrder']+1;
?>
<?php echo $this->Form->create('UserCaseStudyAnswer', array('action' => 'mark_done'));?>
<?php echo $this->Form->hidden('case_study_resource_id', array('value' => $rid));?>
<?php echo $this->Form->hidden('Next', array('value' => $next));?>
<?php echo $this->Form->textarea('Answer', array('label' => 'Your Response'));?>
<?php echo $this->Form->submit('Save My Answer');?>
<?php echo $this->Form->end();?>
