<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>
<h2>Case Study</h2>
Today, you arrive at Mrs. Carter's home and notice that she seems very withdrawn and quiet.  You give her privacy for the morning, but one hour before you are scheduled to leave, you see that she has tears in her eyes.   She denies any pain but starts to tell you about her husband who died 6 months ago.  She seems to perk up as she talks about him and she asks you to stop doing your work and sit with her.  You have not finished your work duties for the day, but you realize that talking about her husband is helping to cheer her up.
<ul>
	<li>Do you stop working for the last hour of your shift and sit with her?</li>
	<li>Do you tell her that you are not allowed to stop working, not allowed to take a seat?</li>
	<li>What reason do you have for the decision that you make?</li>
</ul>

<?php echo $this->Form->create('UserCaseStudyAnswer', array('action' => 'mark_done'));?>
<?php echo $this->Form->hidden('case_study_resource_id', array('value' => $rid));?>
<?php echo $this->Form->hidden('Next', array('value' => $tcm_next));?>
<?php echo $this->Form->textarea('Answer', array('label' => 'Your Response'));?>
<?php echo $this->Form->submit('Save My Answer');?>
<?php echo $this->Form->end();?>
