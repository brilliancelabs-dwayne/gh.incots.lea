<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>
<h2>Case Study</h2>
Your patient is on bedrest and has a Stage I decubitus ulcer. The care plan instructions indicate that the patient is to be turned and repositioned every two hours. You make sure to follow the instructions but the bedsore is looking worse and you are quite sure that the family members, or the aide that relieves you, is not turning and re-positioning Mr. Lopez as directed.
	<li>Do you devise a way to document how often Mr. Lopez is turned and re-positioned? If so, what is your plan.</li>
	<li>Do you just do what you are supposed to and not worry about it, because you won’t be responsible if the bedsore worsens?
	<li>Do you confront the family and the other aide about it? If so, do you mention that the bedsore is worse?</li>
	<li>Is this an issue that you would call your supervisor about and/or document on your paperwork?</li>
</ul>

<?php echo $this->Form->create('UserCaseStudyAnswer', array('action' => 'mark_done'));?>
<?php echo $this->Form->hidden('case_study_resource_id', array('value' => $rid));?>
<?php echo $this->Form->hidden('Next', array('value' => $tcm_next));?>
<?php echo $this->Form->textarea('Answer', array('label' => 'Your Response'));?>
<?php echo $this->Form->submit('Save My Answer');?>
<?php echo $this->Form->end();?>
