<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>
<h2>Case Study</h2>
Your patient, Ms. Jay, who lives alone, is a new diabetic. She loves candy, cakes and all other desserts and is upset that she can’t eat the way she used to. Ms. Jay informs you that today is her birthday and she has ordered a birthday cake to be picked up when you do her grocery shopping.
<p class="sub">Discuss the following:</p>
<ul>
	<li>Do you try to convince her not to buy the cake, and if so, what do you say to convince her?</li>
	<li>Do you ask the bakery to switch out the cake for one that is sugar free, and if so, do you admit it to your patient?</li>
	<li>Do you go along with her request, and if so, is this something you would call your supervisor about and/or document on your paperwork?</li>
</ul>

<?php echo $this->Form->create('UserCaseStudyAnswer', array('action' => 'mark_done'));?>
<?php echo $this->Form->hidden('case_study_resource_id', array('value' => $rid));?>
<?php echo $this->Form->hidden('Next', array('value' => $tcm_next));?>
<?php echo $this->Form->textarea('Answer', array('label' => 'Your Response'));?>
<?php echo $this->Form->submit('Save My Answer');?>
<?php echo $this->Form->end();?>
