<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>
<h2>Case Study</h2>
Your patient has you read her mail to her everyday because of her poor eyesight. Today a letter has arrived from her oldest son in Texas. In the letter, he tells her that he is not able to come for her 90th birthday because he is very ill with aids. You know that this will upset her a lot and you know she has a history of heart attacks.<p class="sub">Discuss the following:</p>
<ul>
	<li>Do you read the entire letter and let her know about her son’s illness?</li>
	<li>Do you read the letter in part, telling her that her son can’t make it, without giving her the true reason?</li>
	<li>Do you tell her that there are some personal things in the letter and you are not comfortable reading them; suggesting she wait until her granddaughter comes home from work to read it to her?</li>
</ul>

<?php echo $this->Form->create('UserCaseStudyAnswer', array('action' => 'mark_done'));?>
<?php echo $this->Form->hidden('case_study_resource_id', array('value' => $rid));?>
<?php echo $this->Form->hidden('Next', array('value' => $tcm_next));?>
<?php echo $this->Form->textarea('Answer', array('label' => 'Your Response'));?>
<?php echo $this->Form->submit('Save My Answer');?>
<?php echo $this->Form->end();?>
