<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>
<style>
<!--
div.info div {
	padding: 15px;
	
}

-->
</style>
<div class="info">	
<h2>What to Bring</h2>
<ul>
	<li>Wear uniform (scrubs) of any color</li>
	<li>A watch with a second hand</li>
	<li>Small note pad and 2 pens</li>
	<li>Tennis shoes or nursing shoes (No open toe shoes/no sandals of any sort)</li>
	<li>Lunch (There is a refrigerator to store your lunch and
also a microwave to heat) (Coffee, tea and water are
free) Trainees who prefer to go out for lunch, will have
adequate time. Harvard Park shopping complex has
numerous food vendors</li>
</ul>

</div>
