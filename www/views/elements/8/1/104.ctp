<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>
<style>
<!--
div.info div {
	padding: 15px;
	
}

-->
</style>
<h1>Transitioning from Online to Skills Lab Training</h1>
<div class="info">

<h2>Skills Lab Details</h2>
<div>
<h3>Lab location:</h3>
	<div>
	inLab - Spectrum Home Health Care <br>
	2000 Auburn Dr., Suite 200 <br>
	Beachwood, Ohio 44122 <br>
	</div>
	<div>
	You will meet your Nurse Instructor in the lobby of the
	building.
	</div>

<h3>Lab Times:</h3> 
<div>
Saturday, June 2, 2012 9 am to 5 pm <br>

Sunday, June 3, 2012 9 am to 5 pm <br>
</div>
</div>

