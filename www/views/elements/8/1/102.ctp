<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>

<h1>HEALTHCARE ISSUES & THE CARE TEAM</h1>

<h2>The Rights of the Home Healthcare Patient</h2>

<ul>
	<li>Define the care plan, its purpose and your role in following it.</li>
	<li>Explain how to report abuse and neglect.</li>
	<li>Name 5 suspicious injuries that should be reported to your supervisor.</li>
	<li>Define the term communication and explain why feedback is an important part of communication.</li>
	<li>List ways to make communication accurate.</li>
	<li>Define what an incident report is and be able to give examples to when it is necessary to complete one.</li>
	<li>Identify the patient's rights in home health care.</li>

</ul>

<h2>Infection Control</h2>

<ul>
	<li>Explain why the elderly are at a higher risk for infection.</li>
	<li>Identify symptoms of an infection.</li>
	<li>Explain the term "hand hygiene" and identify when it is necessary to wash your hands.</li>
	<li>Describe the Centers for Disease Control and Prevention and what their role is.</li>
	<li>Explain standard precautions.</li>
	<li>Be able to list guidelines for handling equipment and linen.</li>
	<li>Explain transmission-based precautions.</li>
	<li>Explain how to handle spills in your patient's home.</li>
	<li>Define bloodborne pathogens and describe 2 major bloodborne diseases.</li>

</ul>


<h2>Patient Safety</h2>

<ul>
	<li>Identify those persons at greatest risk for accidents.</li>
	<li>Describe accident prevention guidelines.</li>
	<li>List safety guidelines for oxygen use.</li>
	<li>Identify major causes of fire in the home.</li>
	<li>List fire safety guidelines.</li>

</ul>


<h2>Human Needs and Human Development</h2>

<ul>
	<li>List 6 basic physical needs that all humans have.</li>
	<li>List 6 psychosocial needs that humans have.</li>
	<li>Understand and be able to list Maslow's Hierarchy of Needs.</li>
	<li>Be able to describe the need for activity for your patient.</li>
	<li>Name 3 truths about the aging process.</li>
	<li>Name 3 myths about the aging process.</li>
	<li>Describe the important of good nutrition</li>

</ul>


<h2>The Healthy Human Body</h2>

<ul>
	<li>Be able to describe what makes up the integumentary system.</li>
	<li>Be able to describe the musculoskeletal system.</li>
	<li>Be able to describe the urinary system.</li>
	<li>Be able to describe the respiratory system.</li>
	
</ul>


<h2>Body Mechanics and Positioning/Lifting/Moving Patients</h2>

<ul>
	<li>Body Mechanics and Positioning/Lifting/Moving Patients</li>
	<li>Explain the different types of positioning.</li>
	<li>Describe how to safely position your patient.</li>
	<li>Describe how to safely transfer your patient.</li>
	<li>Describe how to safely ambulate your patient.</li>
	
</ul>








