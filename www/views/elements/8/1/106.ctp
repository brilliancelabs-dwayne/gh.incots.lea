<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>
<style>
<!--
div.info div {
	padding: 15px;
	
}

-->
</style>
<h1>Day 1 Curriculum</h1>

<h2>Preventing Infection:</h2>
<ul>
	<li>Washing Hands</li>
	<li>Putting on gloves</li>
	<li>Taking off gloves</li>
	<li>Putting on a gown</li>
	<li>Putting on mask and goggles</li>
</ul>

<h2>Positioning, Lifting, and Moving</h2>
<ul>
	<li>Moving a patient up in bed</li>
	<li>Moving a patient to the side of the bed</li>
	<li>Turning a patient</li>
	<li>Logrolling a patient with help from one assistant</li>
	<li>Assisting patient to sit up on side of bed, dangling</li>
	<li>Applying a transfer belt</li>
	<li>Transferring a patient from bed to wheelchair</li>
	<li>Transferring a patient to bedside commode</li>
	<li>Transferring a patient into a car (Talk through only)</li>
	<li>Assisting a patient to ambulate</li>
	<li>Assisting a patient to ambulate using walker</li>
	<li>Assisting a patient to ambulate using cane</li>
	<li>Assisting a patient to ambulate using crutches</li>
</ul>

<h2>Personal Care Skills</h2>
<ul>
	<li>Making an occupied bed</li>
	<li>Making an unoccupied bed</li>
	<li>Giving a complete bed bath</li>
	<li>Shampooing hair (Talk through)</li>
	<li>Giving a shower or tub bath (Talk through)</li>
	<li>Providing foot care</li>
	<li>Shaving a patient</li>
	<li>Putting on TED hose</li>
</ul>

<h2>Urinary Elimination</h2>
<ul>
	<li>Assisting a patient with the use of a bedpan</li>
	<li>Assisting a male patient with the use of a urinal</li>
	<li>Assisting a patient to use bedside commode</li>
	<li>Providing perineal care for an incontinent patient</li>
	<li>Providing cathether care (Talk through)</li>
	<li>Emptying the catheter drainage bag (Talk through)</li>
</ul>
