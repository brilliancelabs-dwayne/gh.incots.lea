<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>
<style>
<!--
div.info div {
	padding: 15px;
	
}

-->
</style>
<h1>Day 2 Curriculum</h1>

<h2>Emergency Care</h2>
<ul>
	<li>Performing abdominal thrusts for the conscious person</li>
	<li>Clearing an obstructed airway in a conscious infant</li>
	<li>Responding to shock</li>
	<li>Responding to a heart attack</li>
	<li>Controlling bleeding</li>
	<li>Responding to poisoning</li>
	<li>Treating burns</li>
	<li>Responding to fainting</li>
	<li>Responding to a nosebleed</li>
	<li>Responding to seizures</li>
	<li>Responding to vomiting</li>
</ul>

<div class="info">
<h3>Special note</h3>
<div>
Trainees will be instructed on signs/symptoms of hypo-/hyperglycemia; recognizing when a patient is displaying a change in mental
status, recognizing s/s of elder abuse and finally, discuss those situations
whereby a HHA should notify their supervisor at once.
</div>

<h2>Personal Care</h2>
<ul>
	<li>Combing or brushing hair</li>
	<li>Dressing a patient with an affected right arm</li>
	<li>Providing oral care</li>
	<li>Providing oral care for the unconscious patient</li>
	<li>Flossing teeth of a patient</li>
	<li>Cleaning and storing dentures</li>
</ul>


<h2>Basic Nursing Skills</h2>
<ul>

	<li>Taking and recording an oral temperature</li>
	<li>Taking and recording a rectal temperature (Talk through)</li>
	<li>Taking and recording a tympanic temperature</li>
	<li>Taking and recording an axillary temperature</li>
	<li>Taking and recording apical pulse</li>
	<li>Taking and recording radial pulse and counting respirations</li>
	<li>Taking and recording blood pressure</li>
	<li>Feeding a patient who cannot feed themselves</li>
	<li>Measuring and recording intake and output</li>
	<li>Infection Prevention and Safety in the Home:</li>
	<li>Cleaning a bathroom (Talk through)</li>
	<li>Doing Laundry (Talk through)</li>
</ul>

