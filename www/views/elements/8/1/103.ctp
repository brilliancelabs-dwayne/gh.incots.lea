<?php require_once APP."views/elements/_mapData.ctp";
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>

<h1>CORE SKILLS AND PERSONAL CARE</h1>

<h2>Bathing and Skin Care</h2>
<ul>
	<li>Identify guidelines for providing good skin care.</li>
	<li>Describe measures to take to prevent pressure sores.</li>
	<li>List important observations to make about changes in your patient's skin.</li>
	<li>Explain guidelines for assisting your patient with bathing.</li>
	<li>Name 2 benefits of bathing, other than keeping your patient clean.</li>
	<li>Think of 2 grooming routines that are important to you; do you think these routines would be
important to ill patients?</li>
	<li>Identify guidelines for good oral care.</li>

</ul>

<h2>Vital Signs</h2>
<ul>
	<li>Describe what changes in your patient's vital signs can indicate?</li>
	<li>Describe those changes should be immediately reported to a supervisor.</li>
	<li>List the 4 sites for measuring body temperature.</li>
	<li>Name 7 conditions that indicate when you should not take an oral temperature.</li>
	<li>List the guidelines for taking the pulse and respirations of your patient.</li>
	<li>Be able to explain how to take a blood pressure.</li>
	<li>List the questions that you should ask to find out if your patient is in pain.</li>
	<li>Name those symptoms of pain that should be reported to your supervisor.</li>
</ul>

<h2>Common Chronic and Acute Conditions</h2>
<ul>
	<li>Describe common diseases/disorders of the integumentary system.</li>
	<li>Describe common diseases/disorders of the musculoskeletal system.</li>
	<li>List 5 signs/symptoms of congestive heart failure.</li>
	<li>Describe the difference between Type 1 diabetes and Type 2 diabetes.</li>
	<li>Know the definition of dementia.</li>
	<li>List strategies for better communication for patients with Alzheimer's disease.</li>
	

</ul>


<h2>Mental Health and Mental Illness</h2>
<ul>
	<li>Identify 7 characteristics of mental health.</li>
	<li>Define mental health.</li>
	<li>Identify 4 causes of mental illness.</li>
	<li>Define mental illness.</li>
	<li>Be able to explain your role in caring for patients who are mentally ill.</li>
	<li>Identify important observations that should be made and reported to your supervisor for your patient who is mentally ill.</li>

</ul>


<h2>The Home Care Setting</h2>
<ul>
	<li>Explain the purpose of and need for home care.</li>
	<li>Describe a typical home health agency.</li>
	<li>Explain how working for a home health agency is different from working in other types of healthcare settings.</li>
	<li>Describe your role as a home health aide.</li>
	<li>List typical tasks performed by home health aides.</li>

</ul>


<h2>Medications in Home Care</h2>
<ul>
	<li>List 4 guidelines for safe and proper use of medications.</li>
	<li>Identify the 5 rights of medications.</li>
	<li>Explain how to assist your patient with self-administered medications.</li>
	<li>Identify observations about medications that should be reported immediately to your supervisor.</li>
	
</ul>


<h2>Key Terms/Videos</h2>
<ul>
	<li>Please review all key terms found in your textbook.</li>
	<li>Please review all skills demonstrated in the online videos.</li>

</ul>

