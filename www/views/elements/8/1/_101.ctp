<h2>Welcome to the Course!</h2>

<h3>Economic Enhancement Sponsorships</h3>
<p>Chances are, you are here because of the Economic Enhancement Sponsorship (EES) program.  The EES program seeks to create jobs in this community by training job seekers for gainful employment as a Home Care provider.  This non-profit in conjunction with Incots, LLC have created a training course that will help you learn and practice the skills you will need to not only get a job in the home health care field but to also thrive as a home care provider.</p>

<h3>Structure of the Training</h3>
<p>Even though this training course is conducted and managed online since it is a medical certification course there is a require practical training that must occur in-person.  The breakdown of the training is as follows:
<ul class="main">
	<li><h4>Online Training (50-hours)</h4>
		<ul class="sub">
			<li>Reading selections taken from the <a href="http://www.hartmanonline.com/phc/hhah.htm" target="blank">Home Health Aide Handbook</a></li>
			<li>Instructional skills videos show many of the core required skills that will be executed in the skills labs</li>
			<li>Case studies will open your mind to the type of situations that may occur while on the job.  Don't worry, even though answers are required they're not graded.</li>
			<li>Quizzes mark the end of each training module.  You should be able to recall the information presented in the module.  If you are not then further explanation will be made available to you to improve your understanding of the material.</li>
			<li>The Final exam will be taken offline to ensure that we are certifying the correct trainee</li>
			<li><h5>Twitter Discussions & Support</h5>
				<ul>
					<li>Discuss topics live with other trainees</li>
					<li>Read previous tweets from other trainees and instructors</li>
					<li>Receive live assistance from staff nurses and tech support</li>
				</ul>
			</li>
		</ul>
	</li>
	<li><h4>Skills Lab Training (16-hours)</h4>
		<ul class="sub">
			<li>PUT SKILLS LAB DETAILS HERE</li>
		</ul>
	</li>

</ul>
</p>

<h3>Help Needed!</h3>
<p>This training program has been recently developed and is still in beta testing which means that we appreciate any input that you can provide.  Please let us know if you have any questions or comments on the presentation of the training.</p>

<h3>Terms of Service</h3>
<?php $terms = $course_id.'/1/'.'fb_tos';?>
<textarea rows="15" cols="50" disabled=disabled>
<?php echo $this->element($terms);?>
</textarea>
<p>Please click the "I agree" button on this page to signify that you have read and agree to all of the things mentioned on this page.</p>
