<?php 
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>

<style>
   <!--
   .page_header {
      font-size: 26px;
      color: black !important;
   }   
   .video_container {
      padding: 30px;
   }
   -->
</style>
<script type="text/javascript">
$(document).ready(function(){
   $("youtube").css({
      
   });
   $("video").click(function(e){
      e.preventDefault();
      $(this).hide();
      $("youtube").show();
   });
}); 
</script>
<h2 class="page_header">Watch Video</h2>
<div class = "video_container">
<?php
	//Assuming that an FLV is the default case
	//NOTE: this will change once db values are updated
   $yt = $yts[$rid];
	$flv = $videos[$rid];
	$mp4 = str_replace('flv', 'mp4', $flv);


	//Create the poster image

	$img = preg_replace('/\s+/', '', $element_name);
	$img = strtolower($img);
	$poster = '/resources/course_img/'.$img.'.jpg';

?>

<video id="show_vid" class="video-js vjs-default-skin" controls
  preload="auto" width="640" height="480" poster="<?php echo $poster;?>"
  data-setup="{}">
  <!--source src="<?php echo $flv;?>" type='video/flv'-->
  <!--source src="<?php echo $mp4;?>" type='video/mp4'-->
</video>
<youtube style="display:none">
   <?php echo $this->Media->youtube($yt, 640,480);?>
</youtube>

</div>
<?php
/**/
	echo $this->Html->css('http://vjs.zencdn.net/c/video-js.css', null, 
		array('inline' => false)
	);
 
?>
<?php 
/*
	echo $this->Html->script('http://vjs.zencdn.net/c/video.js', 
		array('inline' => false)
	);
 */ 
?>
