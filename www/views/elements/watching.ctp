<h2 class="page_header">Watch Video</h2>
<div class = "video_container">
<?php
	//Assuming that an FLV is the default case
	//NOTE: this will change once db values are updated
	$flv = $videos[$rid];
	$mp4 = str_replace('flv', 'mp4', $flv);


	//Create the poster image

	$img = preg_replace('/\s+/', '', $element_name);
	$img = strtolower($img);
	$poster = '/resources/course_img/'.$img.'.jpg';

?>

<video id="show_vid" class="video-js vjs-default-skin" controls
  preload="auto" width="640" height="480" poster="<?php echo $poster;?>"
  data-setup="{}">
  <source src="<?php echo $flv;?>" type='video/flv'>
  <source src="<?php echo $mp4;?>" type='video/mp4'>
</video>


</div>
<?php
	echo $this->Html->css('http://vjs.zencdn.net/c/video-js.css', null, 
		array('inline' => false)
	);
?>
<?php 
	echo $this->Html->script('http://vjs.zencdn.net/c/video.js', 
		array('inline' => false)
	);
?>
