<?php 
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	//Set next element for nav purposes
	if(isset($tcm[$arr_num+2])){
		$next = $arr_num+2;

	}else{
		$next = $arr_num;
	}

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>
<style>
<!--
.rounded {
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
}
#status_box {
	width: 500px;
	background: #FFF;
	color: #000;
	padding: 10px;
	margin: auto;
	border: solid 2px yellowgreen;
}
#status_box table td.hcol {
	text-align: left;
	width: 15%;
	white-space: nowrap;
}
a.qs {
	color: black;
	text-decoration: underline;
}
a.qs:hover {
	color: yellowgreen;
}
.qs {
	color: yellowgreen;
}

#quiz_timer {
	position: fixed;
	bottom: 5px;
	left: 5px;
	padding: 15px;
	background: black;
	color: white;
	font-size: 2em;
}

#quiz_container {
	background: #FFF;
	padding: 25px;
	width: 720px;
	color: #000;
	border: solid thin #777;
	margin: auto;
	margin-top: 25px;
}
.question_container {
	padding: 5px;
	margin: 15px;
	margin-bottom: 25px;
}
.qnum {
	border-right: solid thin #777;
	font-weight: normal;
	font-size: 1.75em;
	margin: 10px;
	float: left;
	padding: 0 5px;

}
.question_text {
	text-align: left;
	text-transform: justify;
	text-indent: 0px;
	padding: 3px 45px 0px;
	line-height: 1.4em;
	clear: both;
}

.tf_container, .mc_container {
	line-height: 55px;

}
select.tf, select.mc {
	
	padding: 2px;
	font-size: 1.25em;
	background: white;
}

#quiz_action_container {
	padding: 5px;
	text-align: center;
}
.qa a {
	font-size: 1.2em;
	padding: 15px 45px;
	background: #CCC;
	color: black;
}
.qa a:hover {
	background: yellowgreen;
	color: white;
}
-->
</style>

<script>
$(document).ready(function(){

	$('#qs_start').click(function(e){
		e.preventDefault();
		$('#quiz_container').toggle();
		$('#quiz_timer').toggle();
		$(this).html('In progress...');
		

		/**
		* The Quiz Timer
		*
		**/
		window.interval = $('#quiz_timer').html();
	
		$('#quiz_timer').html('loading timer...');	

		$('#quiz_timer').html(window.interval+'-mins');		

		window.setInterval(yourfunction, 60000);
		
			function yourfunction() {
				if(window.interval>-1){
					if(window.interval<=5){
						$('#quiz_timer').css('background', 'red');
					}
					$('#quiz_timer').html(window.interval+'-mins');
					window.interval--;
				}else{
					$('#quiz_timer').html("time's up!");	
					$('#quiz_container').hide();
					$('#qs_start').html('Grading...');
	
					cont = confirm('Sorry, your time is up.  Please click here to submit your quiz.');
					//$('form').submit();
					
				}

			}
		});

			/**
			* Quiz Validation and Submission
			**/
			$('#grade a').click(function(e){



				var valid = 0;

				e.preventDefault();

				var form_elements = $('.fe').length;

				var quiz_size = $('.quiz_length').html();

				//Find all multi-word answers and prepare them concatenation
				// in the user_quiz_responses controller
				var i = 1;				

				//since we are cycling through quiz questions we will validate too
				var empty_values = 0;
				var valstring = '';

				var ids = $('form').find('input').map(function(){					

					if($(this) && $(this).val()!="POST"){
						
						//make a string of values
						valstring+=$(this).val();

						//add  up the number of empty values
						if($(this).val()=="" ||
							$(this).val()=="Choose the best answer"
						){
							empty_values += 1;

						}else{
							//not empty
						}	
						

						/**vF(x)**/
						//if the id of this element and the next are the same
						if($(this).next().attr('id')==$(this).attr('id')){
							//append the unique i value to the name of this element
							var field_name = 'data[UserQuizResponse][';
							var new_name = 'tmp'+i+'_'+i;
							field_name +=new_name+']';

							$(this).attr('name', field_name);

						}else{
							//change the name to tmp, to be parsed later in the controller
							$(this).attr('name', 'data[UserQuizResponse][tmp'+i+']');
							i++;
						}


					}
					
					
				}).get();

				//Tabulate the fill in (input answers)
				if(empty_values!=0){
					//$('form').submit();
					alert('You have left '+empty_values+' fill-in boxes empty.');
			
					//Give the user the chance to override the error and continue
					var cont = confirm('Would you like to continue?');
				}else{

					//All fill-in answers are completed so continue to next step
					var cont = 1;

				}

				/**Now the select answer types which are T/F and Multiple Choice
				*****This should exist but it doesn't
				**/
				
				/**Submit if approved**/
				if(cont){
					alert('Submitting exam for grading');
					$('form').submit();
				
				}else{
					alert('Exam not submitted, please continue.');	
				}


			})
		
/**/
});
</script>
<?php 

//initialize
$qq_init = 1;


foreach($quizzes as $q){
	$qr = $q['QuizResource'];

	if($qr['id']==$rid){
		$quiz = $q;
		$quiz_rules = $q['QuizRule'];
		$quiz_length = count($quiz['QuizQuestion']);
		$quiz_minscore = $quiz_rules['PercentageCorrect']*100;

		$qq_init = $q['QuizQuestion'];
	}
}


/**
* Remove the underscores and replace with input boxes
**/
$i = 1; //start quiz questions at #1

if(is_array($qq_init)):
	foreach($qq_init as $qq){
		$qtemp = $qq['Question'];
		$questions[$i] = $qq;
		$questions[$i]['Num'] = $i;
		$i++;
	}
endif;
?>


<?php 
/**
** The Quiz Header
**/

if(!isset($quiz_rules)):
	$quiz_rules['TimeLimit'] = 'N/A';
	$quiz_length = 'N/A';
	$quiz_minscore = 'N/A';

endif;
?>
<h2 class="page_header">Take a Quiz</h2>
<div id="status_box" class="rounded">
	<table>
		<tr>
			<td class="hcol">Time Limit:</td>
			<td><?php echo $quiz_rules['TimeLimit'];?></td>
			<td class="hcol">Questions:</td>
			<td class="quiz_length"><?php echo $quiz_length;?></td>
		</tr>
		<tr>
			<td class="hcol">Min Score:</td>
			<td><?php echo $quiz_minscore;?>%</td>
			<td class="hcol">Status:</td>
			<td class="quiz_status">
			<?php 
				//if($map['Completed']==0):
				##Hack to turn off the re-take lock
				if(1==1):
			?>
				<a href="#" class="qs" id="qs_start">Begin</a>

			<?php elseif($map['Completed']==1):?>
				<div class="qs">Awaiting Grade</div>
			<?php endif;?>
	
			</td>
		</tr>
	</table>
</div>

<?php
/**
** Quiz Timer
**/
?>
<div id="quiz_timer" style="display: none;">
<?php echo $quiz_rules['TimeLimit'];?>
</div>
<?php $next_url = $rid."/".$next;?>
<?php 
//Changed "grade" to "finalexam" as a HACK
echo $this->Form->create('UserQuizResponse', array('action' => 'finalexam/'.$next_url));?>
<div id="quiz_container" class="rounded"  style="display: none;">
<?php 
/**Question Formatting**/
$qn = 1;
if(!isset($questions) || !is_array($questions)):
?>
<h3>Quiz not available</h3>

<?php
else:

foreach($questions as $ques):	
	$qtype = $ques['question_type_id'];
?>

<!--START Question Container-->
	<div class="question_container">
		<span class="qnum"><?php echo $ques['Num'];?></span>

<?php
	switch($qtype):
	
		//Multiple Choice
		case 1:
		$mc_options = null;
		$mc_options[] = 'A';
		$mc_options[] = 'B';
		$mc_options[] = 'C';
		$mc_options[] = 'D';
		$mc_options[] = 'E';
		$mc_options[] = 'F';


		//How many options are given for this question?
		$o_count = 0;
		$option_string = null;
		for($i=1;$i<10;$i++){

			//Formatting the option string
			$option = 'Option'.$i;	

			if(isset($ques[$option]) && $ques[$option]!=""){
				$o_count = $i;
			}
		}
?>
		<span class="mc_container">
			<select class="mc fe" name="data[UserQuizResponse][<?php echo $qn;?>]">
			<option value="null" selected="selected">Choose the best answer</option>
				
<?php
		//Get the options for this question
		for($o=1;$o<=$o_count;$o++):
			$option_string .= '<option value="'.$mc_options[$o-1].'">'.$ques["Option".$o].'</option>'. "/n/r";
		endfor;
	
		echo $option_string;
?>
			</select>
		</span>



<?php
		break;
	
		//True or False
		case 2:
?>
		<span class="tf_container">
			<select class="tf fe" name="data[UserQuizResponse][<?php echo $qn;?>]">
				<option value="null" selected="selected">True or False</option>
				<option value="T">True</option>
				<option value="F">False</option>
			</select>
		</span>
<?php
		break;

		//Fill in the blank
		case 3:
		
		$qtemp = $ques['Question'];
		
		//replace underline with an input box for fill in questions
		$q = preg_replace('/[_]+/', '<input type="text" class="fillin fe" id="'.rand(1111,9999999).'" name="data[UserQuizResponse]['.$qn.']" />', $qtemp);
		
		//replace the question in the quiz question array with the newly formatted value
		$ques['Question'] = $q;




		break;


		//Essay
		case 4: 

		break;


		//Case study
		case 5:

		break;
		

		//Error		
		default:

	endswitch;
?>
		<div class="question_text"><?php echo $ques['Question'];?></div>

	</div>
<!--END Question Container-->
<?php
	$qn++;
endforeach;

endif;

?>
<?php if(!empty($questions)):?>
<div id="quiz_action_container">
	<span id="grade" class="qa"><?php echo $this->Html->link('Grade', '#');?></span>
</div>

<?php endif;?>
</div>
<?php echo $this->Form->end();?>
