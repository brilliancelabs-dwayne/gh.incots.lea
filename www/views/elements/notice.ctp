<?php 
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>

<?php
//Build the link to the static notice page
//the number 2 is hardcoded b/c that is the element type ID for notices
$notice = $course_id.'/2/'.$rid;
?>
<div class="para">
<?php echo $this->element( $notice );?>

</div>
