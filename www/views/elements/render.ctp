<?php

	//This fix will create a variable which can be checked before loaded content-specific layout structures
	$isModule = true;
	//module refers to whether or not a training module has been loaded

	if(isset($this->params['pass'][0])):

		$action = $this->params['pass'][0];

		if(!is_numeric($action)):
			$isModule = false;

		endif;

	endif;
?>
<style>
<!--
/*Agreements*/
div.para {
	padding: 25px;
}
div.para h2 {
	font-size: 1.75em;
}
div.para h3 {
	padding: 15px 5px 5px;
	font-size: 1.25em;
	letter-spacing: -2px;
	font-weight: bold;
	color: yellowgreen;
}

div.para li h4 {
	font-weight: bold;
	font-size: 1.15em;	
}
h5 {
	font-weight: bold;
	color: #777;
}
div.para p {
	padding: 5px;
	max-width: 750px;
}

ul.main li {
	padding: 5px 15px;
	list-style-type: none;
}

div.para ul.sub li {
	padding: 5px 15px;
	list-style: disc inside none;
}
-->
</style>
<?php 
	//Change current to array style number
	$order_num = $tcm_current - 1;
?>

<?php
	//Grab the current map element
	$module = $tcm[$order_num]['TrainingCourseMap'];

	//Map element determines the type of action that will be used to view the element content
	$map_element_id = $module['map_element_id'];
	
?>


<?php 
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current;

	if($tcm_current!=""):
		$arr_num = $tcm_current-1;
	endif;
	
	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//Has this element been completed by the trainee?
	$element_completed = $map['Completed'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>

<?php

	//Set the action associated with the map element types
	switch($map_element_id):
	
		case 1:
		$type = 'Agreement';
		$action = 'agree';
		$mad_msg = "Done";
		$controller = "user_agreement_responses";
		$model = 'UserAgreementResponse';		
		$ridn = 'agreement_resource_id'; //resource id name
		break;

		case 2:
		$type = 'Notice';
		$action = 'notice';
		$mad_msg = "I understand";
		$controller = "user_notice_responses";
		$model = 'UserNoticeResponse';
		$ridn = 'notice_resource_id';
		break;

		case 3:
		$type = 'Reading';
		$action = 'read';
		$mad_msg = "I'm done reading";
		$controller = "user_reading_responses";
		$model = 'UserReadingResponse';
		$ridn = 'reading_resource_id';
		break;

		case 4:
		$type = 'Case Study';
		$action = 'consider';
		$mad_msg = "XX";
		$controller = "user_case_study_responses";
		$model = 'UserCaseStudyAnswer';
		$ridn = 'case_study_resource_id';
		break;

		case 5:
		$type = 'Quiz';
		$action = 'take';
		$mad_msg = "Submit Quiz";
		$controller = "user_quiz_responses";
		$model = 'UserQuizResponse';
		$ridn = 'quiz_resource_id';
		break;

		case 6:
		$type = 'Video';
		$action = 'watch';
		$mad_msg = "I've watched";
		$controller = "user_video_responses";
		$model = 'UserVideoResponse';
		$ridn = 'video_resource_id';
		break;

		default:
		$type = 'error';
		$action = 'throw';
		$mad_msg = "Report Error to IT";
		$controller = "train";
		$model = null;
		$ridn = null;
	
	endswitch;

?>
	<?php
	//Build the Mark as Done link
	$msg = 'Are you sure you want to submit this training element as "COMPLETED"?';
	
	$MaD = $this->Html->link($mad_msg, array('controller' => $controller, 'action' => 'mark_done', $map_element_id, $rid, $tcm_next), array(
		'class' => 'mark_done'
	), $msg);

	?>

	<?php 
	if(!$element_completed && $map_element_id !=4 && $map_element_id != 5):?>
	<span class="done">
		<?php echo $MaD;?>
	</span>
	<?php else:
		$tcm_next -= 1;


	?>
	<?php endif;?>

<?php 
if(isset($isModule)):
	echo $this->element($action);
endif;
?>
