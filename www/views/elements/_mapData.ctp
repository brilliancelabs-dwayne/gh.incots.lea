<?php  
	//Change current to array style number
	$order_num = $tcm_current - 1;

	//Grab the current map element
	$module = $tcm[$order_num]['TrainingCourseMap'];

	//Map element determines the type of action that will be used to view the element content
	$map_element_id = $module['map_element_id'];
	

/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>

<?php

	//Set the action associated with the map element types
	switch($map_element_id):
	
		case 1:
		$type = 'Agreement';
		$action = 'agree';
		$mad_msg = "I agree";
		$controller = "user_agreement_responses";
		$model = 'UserAgreementResponse';		
		$ridn = 'agreement_resource_id'; //resource id name
		break;

		case 2:
		$type = 'Notice';
		$action = 'notice';
		$mad_msg = "I understand";
		$controller = "user_notice_responses";
		$model = 'UserNoticeResponse';
		$ridn = 'notice_resource_id';
		break;

		case 3:
		$type = 'Reading';
		$action = 'read';
		$mad_msg = "I'm done reading";
		$controller = "user_reading_responses";
		$model = 'UserReadingResponse';
		$ridn = 'reading_resource_id';
		break;

		case 4:
		$type = 'Case Study';
		$action = 'consider';
		$mad_msg = "I'm done";
		$controller = "user_case_study_responses";
		$model = 'UserCaseStudyAnswer';
		$ridn = 'case_study_resource_id';
		break;

		case 5:
		$type = 'Quiz';
		$action = 'take';
		$mad_msg = "Submit Quiz";
		$controller = "user_quiz_responses";
		$model = 'UserQuizResponse';
		$ridn = 'quiz_resource_id';
		break;

		case 6:
		$type = 'Video';
		$action = 'watch';
		$mad_msg = "I've watched";
		$controller = "user_video_responses";
		$model = 'UserVideoResponse';
		$ridn = 'video_resource_id';
		break;

		default:
		$type = 'error';
		$action = 'throw';
		$mad_msg = "Report Error to IT";
		$controller = "train";
		$model = null;
		$ridn = null;
	
	endswitch;

?>

