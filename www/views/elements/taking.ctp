<style>
<!--
.rounded {
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
}
#status_box {
	width: 500px;
	background: #FFF;
	color: #000;
	padding: 10px;
	margin: auto;
	border: solid 2px yellowgreen;
}
#status_box table td.hcol {
	text-align: left;
	width: 15%;
	white-space: nowrap;
}
a.qs {
	color: black;
	text-decoration: underline;
}
a.qs:hover {
	color: yellowgreen;
}
.qs {
	color: yellowgreen;
}

#quiz_timer {
	position: fixed;
	bottom: 5px;
	left: 5px;
	padding: 15px;
	background: black;
	color: white;
	font-size: 2em;
}

#quiz_container {
	background: #FFF;
	padding: 25px;
	width: 720px;
	color: #000;
	border: solid thin #777;
	margin: auto;
	margin-top: 25px;
}
.question_container {
	padding: 5px;
	margin: 15px;
	margin-bottom: 25px;
}
.qnum {
	border-right: solid thin #777;
	font-weight: normal;
	font-size: 1.75em;
	margin: 10px;
	float: left;
	padding: 0 5px;

}
.question_text {
	text-align: left;
	text-transform: justify;
	text-indent: 0px;
	padding: 3px 45px 0px;
	line-height: 1.4em;
	clear: both;
}

.tf_container {
	line-height: 55px;

}
select.tf {
	
	padding: 2px;
	font-size: 1.25em;
	background: white;
}

#quiz_action_container {
	background: royalblue;
	padding: 50px;
	text-align: center;
}
.qa {
	
	font-size: 2em;
	padding: 15px 45px;
	background: #EEE;
}
.qa a {
	color: black;
}
.qa a:hover {
	color: yellowgreen;
}
-->
</style>

<script>
$(document).ready(function(){

//Check for completion stats
//alert('running');
var rid = '<?php echo $rid?>';
var next = '<?php echo $next?>';

$.ajax({
	'url': '/user_quiz_responses/get_score/'+rid+'/'+next,
	'error': function(){},
	'beforeSend': function(){},
	'complete': function(data){
		var QuizStatus = data.responseText;
		window.qs = QuizStatus;

		if(QuizStatus!=0){
			$('.qs').html(QuizStatus);		
		}else{
			console.log(data);
		}

	}
	
});

//alert('done');





	$('#qs_start').click(function(e){
		e.preventDefault();
		$('#quiz_container').toggle();
		$('#quiz_timer').toggle();
		$(this).html('In progress...');
		
		window.interval = $('#quiz_timer').html();
	
		$('#quiz_timer').html('loading timer...');	

		$('#quiz_timer').html(window.interval+'-mins');		

		window.setInterval(yourfunction, 60000);
		
			function yourfunction() {

				if(window.interval>-1){

					if(window.interval<=5){
						$('#quiz_timer').css('background', 'red');
					}
					$('#quiz_timer').html(window.interval+'-mins');
					window.interval--;

				}else{

					$('#quiz_timer').html("time's up!");	

					$('#quiz_container').hide();

					$('#qs_start').html('Grading...');
	
					$('form').submit();
					//alert('Sorry, your time is up.  Please click here to continue.');
				}

			}
	});

			$('#grade a').click(function(e){

				var valid = 0;

				e.preventDefault();

				var form_elements = $('.fe').length;

				var quiz_size = $('.quiz_length').html();


				var i = 1;				
				var ids = $('form').find('input').map(function(){
					
					if($(this).val()!="POST"){
						if($(this).next().attr('id')==$(this).attr('id')){
							$(this).attr('name', 'data[UserQuizResponse][tmp'+i+'a]');
						}else{
							$(this).attr('name', 'data[UserQuizResponse][tmp'+i+']');
							i++;


						}


					}
					

				}).get();
				

				//submit
				$('form').submit();
			})

});
</script>
<?php 

foreach($quizzes as $q){
	$qr = $q['QuizResource'];

	if($qr['id']==$rid){
		$quiz = $q;
		$quiz_rules = $q['QuizRule'];
		$quiz_length = count($quiz['QuizQuestion']);
		$quiz_minscore = $quiz_rules['PercentageCorrect']*100;

		$qq_init = $q['QuizQuestion'];
	}
}
/**/
$i = 1; //start quiz questions at #1

foreach($qq_init as $qq){
	
	$qtemp = $qq['Question'];

//	$q = preg_replace('/[_]+/', '<input type="text" class="fillin fe" id="'.rand(1111,9999999).'" name="data[UserQuizResponse]['.$i."--".rand(1111,9999999).']" />', $qtemp);

	$q = preg_replace('/[_]+/', '<input type="text" class="fillin fe"	id="'.$i.'" name="data[UserQuizResponse]['.$qq['id'].']" />', $qtemp);

	$qq['Question'] = $q;
	$questions[$i] = $qq;
	$questions[$i]['Num'] = $i;
	$i++;
}

?>

<h2 class="page_header">Take a Quiz</h2>
<div id="status_box" class="rounded">
	<table>
		<tr>
			<td class="hcol">Time Limit:&nbsp;</td>
			<td><?php echo $quiz_rules['TimeLimit'];?></td>
			<td class="hcol">Questions:&nbsp;</td>
			<td class="quiz_length"><?php echo $quiz_length;?></td>
		</tr>
		<tr>
			<td class="hcol">Min Score:&nbsp;</td>
			<td><?php echo $quiz_minscore;?>%</td>
			<td class="hcol">Status:&nbsp;</td>
			<td class="quiz_status">


			<?php 
			###Hack
			$map['Completed'] = 0;
			if($map['Completed']==0):?>
				<a href="#" class="qs" id="qs_start">Begin</a>

			<?php elseif($map['Completed']==1):?>
				<div class="qs">Awaiting Grade</div>

			<?php elseif($map['Completed']==2):?>
				<div class="qs">Quiz Passed</div>


			<?php elseif($map['Completed']==3):?>
				<div class="qs">Quiz Failed | <a href="#" class="qs" id="qs_start">Try again?</a></div>


			<?php endif;?>
	
			</td>
		</tr>
	</table>
	
</div>

<?php
//Quiz Timer
?>
<div id="quiz_timer" style="display: none;">
<?php echo $quiz_rules['TimeLimit'];?>
</div>
<?php $next_url = $rid."/".$next;?>
<?php 

echo $this->Form->create('UserQuizResponse', array('action' => 'grade/'.$next_url, 'id' => 'UserQuizResponseForm'));?>
<div id="quiz_container" class="rounded"  style="display: none;">
<?php 
//roll out the questions
$qn = 1;

foreach($questions as $ques):
	//debug($questions);
?>
	<div class="question_container">
		<span class="qnum"><?php echo $ques['Num'];?></span>

		<?php if( $ques['question_type_id'] == 2 ): //2 = T/F ?>
		<span class="tf_container">
			<select class="tf fe" name="data[UserQuizResponse][<?php echo $ques['id'];?>]">
				<option value="null" selected="selected">True or False</option>
				<option value="T">True</option>
				<option value="F">False</option>
			</select>
		</span>
		<?php endif;?>
		<div class="question_text"><?php echo $ques['Question'];?></div>

	</div>
<?php
	$qn++;
endforeach;
?>
<?php if(!empty($questions)):?>
<div id="quiz_action_container">
	<span id="grade" class="qa"><?php echo $this->Html->link('Grade', '#');?></span>
</div>

<?php endif;?>
</div>
<?php echo $this->Form->end();?>
