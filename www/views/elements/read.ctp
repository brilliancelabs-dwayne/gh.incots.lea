<?php 
/***
** PUT this exact code at the top of each action page
**/

	//Turn map order number into array number
	//this snippet first appears on the train-index page
	$arr_num = $tcm_current-1;

	$map = $tcm[$arr_num]['TrainingCourseMap'];

	$element_name = $map['MapElementName'];

	//grab the resource id for the called element
	$rid = $map['resource'];
/***
** END Required header code
**/
?>

<h2 class="page_header">Read</h2>
<?php //echo $this->element('_read');?>

<script>
$(document).ready(function(){

	//url builder
	var base = "http://"+"<?=$user['host'];?>"+"/OHHAC-EES/read/";

	//var url = 'https://docs.google.com/viewer?url=http://demandtraining.org/webroot/OHHAC-EES/read/101.pdf&embedded=true';
	var goog = "https://docs.google.com/viewer?url=";

	var file = "<?php echo $rid;?>.pdf";

	var embed = "&embedded=true";


	var url = goog+base+file+embed;

   var url = "http://54.235.183.247/incots/pdfjs/web/read.php?file=read/"+file;

   //var url = "/pdfjs/web/read.php?file=read/"+file;

	$('#tmp').attr('src', url); 
	
});
</script>

<iframe id="tmp" width="100%" height="750px" src="" scrolling="yes"></iframe>
