<?php echo $this->element('aj_styles');?>
<style>
<!--
.partner_select {

	text-align: center;	
	margin: auto;
	font-size: 0.65em;
	
}
.download_link {
	margin: auto;
	text-align: center;
	padding: 35px;
}
.download_link a{
	font-family: Helvetica, Arial, sans-serif;
	text-decoration: none;
	padding: 25px;
	margin: auto;
	background: #333;
	text-align: center;
	width: 150px;
	color: white;
}
.download_link a:hover {
	background: #777;
}
-->
</style>
<script>
$(document).ready(function(){
	var file = "A100101.pdf";
	$('select').change(function(){

		if($(this).val()!=0){

			var dl_file = "/forms/"+$(this).val()+"/"+file;

			$('#dl_link').attr('href', dl_file);

			$('.download_link').show();

		}else{
			$('#dl_link').attr('href', '#');

			$('.download_link').hide();
		}
	});

});
</script>
<h2 class="page_header">Download a Registration Form</h2>
<div class = "partner_select">
<?php
	$partners[0] = 'Choose One';
	//Resort the new array in numerical order by key
	asort($partners);

	echo $this->Form->select('Non-Profit', $partners, array('empty' => false));
?>
</div>
<div class = "download_link" style="display: none">
<a href="#" target="_blank" id="dl_link">Download</a>
</div>
