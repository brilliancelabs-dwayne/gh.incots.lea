<?php echo $this->element('aj_styles');?>
<link rel="stylesheet" href="/resources/jqtransform/jqtransform.css" type="text/css" media="all" />


<style>
<!--
.rounded {
	radius: 5px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
}
.action_box {
	width: 50%;
	max-width: 500px;
	margin: auto;
	background: #EEE;
	padding: 50px;
	text-align: center;
}
#org,#you {
	background: #FFF;
}
select,input#pass_code, input#pass_code2, input#me {
	text-align: center;
	padding: 15px;
	font-size: 1.5em;
	line-height: 2em;
	width: 100%;
}
input#pass_code,input#pass_code2, input#me {
	width: 100%;
	padding: 5px;
	color: #CCC;
}
.activate.link {
	padding: 15px;
}
.activate.link a {
	color: black;
	font-family: Helvetica, Arial, sans-serif;
	font-weight: bold;
	text-decoration: none;
}
.activate.link a:hover {
	font-weight: normal;
}
input, select {
	border: solid thin skyblue;
}
.question {
	font-size: 10pt;
	vertical-align: super;
	cursor: pointer;
	
}
-->
</style>
<script>
$(document).ready(function(){

	var ctrpt = $(document).width()/2;

	var topmargin = 25;


	//default page header
		//Activate Your Account
	$('.page_header').html();

	//Show a tip
	$('.question').hover(function(){
		if($('.question_data').css('display')=='none'){
			$('.question_data').toggle();
			//Set some positional data
			var qtip = new Object;
			qtip.size = $(document).width()/2;
			qtip.top = 45;
			qtip.left = qtip.size/2;

			$('.question_data').css({
				'position': 'absolute',
				'left': qtip.left,
				'top': qtip.top,
				'width': qtip.size,
				'padding': 5,
				'background': 'lightblue',
				'text-align': 'center',
				'color': 'black',
				'font-size': 12,
				'font-family': 'Arial, sans-serif'
			});
		}
	}, function(){
			//hide the tip
			$('.question_data').toggle();

		});
	
	//Toggle on mouseout
	$('.question').mouseout(function(){

		$('.qtip').toggle();

	});


	



	//Select the org
	$('select#org').change(function(){
		if($(this).val()!="null"){
			$('#PartnerId').val($(this).val());
			//$(this).hide();
			$('select#you').val(0);
			$('select#you').show();
		}
	});

	//Select your name
	$('select#you').change(function(){
		if($(this).val()!="0"){
			$('#PartnerTrainee').val($(this).val());
			
			//hide all selects
			$('select').hide();
			$('.codigo').show();
			$('input#me').show();
				$('input#me').val($('select#you option:selected').html());
			$('input#pass_code').show();
			
		}
	});

	//Go back if me if clicked
	$('input#me').click(function(){
		//hide this div
		$('.codigo').hide();

		//show all selects
		$('select').show();
	});

	//Clear the default pass code
	$('input#pass_code').click(function(){
		if($(this).val()=="Enter Your Pass Code"){
			//create a password box and get rid of the password text box
			var pass = document.createElement('input');
			pass.type = 'password';
			$(pass).attr('id', 'pass_code2');
			$(pass).css('color', 'black');
			$(this).after(pass);
			$(this).remove();
			$(pass).focus();

			//Show the activate link after at least 5 chars have been entered
			$(pass).keyup(function(e){
				e.preventDefault();
				if($(pass).val().length>4){
					$('.activate').show();
				}
			});
		}
	});


	//Grab the partner trainee list using ajax
	$('#org').change(function(e){
		if($(this).val()!="null"){

			var Url = "/partner_trainees/show/"+$(this).val();

			$.ajax({
				'url': Url,
				'error': function(){
					alert('error');
				},
				'success': function(data){
					var datum = $.parseJSON(data);
					$("#null_id").remove();
					$('#you').html('');
					$("#you").prepend("<option value='0' selected='selected'>Pick your name</option>");
					$.each(datum, function(key, index){
						 $("#you").append("<option value='"+key+"'>"+index+"</option>");
					});
				}
			});
		}
	});

	//Transmit the value
	$('.activate').click(function(e){
			e.preventDefault();
			$('#PartnerPassCode').val($('#pass_code2').val());
			$('form').submit();
	});



});
</script>
<h2 class="page_header">Check Registration Status 
<span class="question">(?)</span>
<span class="question_data" style="display: none;" rel=1>Choose your organization then see if your name is in the list.</span>
<span class="question_data" style="display: none;" rel=2>Your pass code should have been provided by your organization.</span>
</h2>
<div class="action_box rounded">
<select id="org">
	<option value="null">Choose your Organization</option>
<?php if(isset($partners)):?>
	<?php foreach($partners as $key=>$value):?>
	<option value="<?php echo $key;?>"><?php echo $value;?></option>
	<?php endforeach;?>
<?php endif;?>
</select>

<select id="you" style="display: none;">
	<option id="null_id" value="null">-------------------------</option>
</select>

<div>
<div class="codigo" style="display: none;">
	<input id="me" value="" />
	<input id="pass_code" value="Enter Your Pass Code" type="text" />

	<div class="activate link" style="display: none;">
	<?php echo $this->Html->link('Activate', array('controller' => 'partners', 'action' => 'activate_proc'));?>
	</div>
</div>

<div class="hidden_vals">
<?php echo $this->Form->create('Partner', array('action' => 'activate_proc'));?>
<?php echo $this->Form->hidden('Id');?>
<?php echo $this->Form->hidden('Trainee');?>
<?php echo $this->Form->hidden('PassCode');?>
<?php echo $this->Form->end();?>
</div>
