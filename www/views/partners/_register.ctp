<?php echo $this->element('aj_styles');?>
<?php if(isset($partner)):?>
<h2 class="page_header"><?php echo $partner;?></h2>
<div class="register_form">
<?php echo $this->Form->create('Partner', array('action' => 'proc_register'));?>
<table>
<tr>
	<td>Full Name:</td>
	<td><?php echo $this->Form->input('FirstName', array('label' => ''));?></td>
	<td></td>
</tr>

<tr>
	<td>Birth Date:</td>
		<td>
			<?php /*<input type="date" name="data[BirthDate]" value="" />*/?>
			<?php echo $this->Form->input('BirthDate', array(
				'type' => 'date', 
				'label' => '',
				'minYear' => '1901',
				'maxYear' => '1994'
));?>
		</td>
	<td></td>
</tr>

<tr>
	<td>Email:</td>
	<td><?php echo $this->Form->input('Email', array('label' => ''));?></td>
	<td></td>
</tr>

<tr>
	<td>&nbsp;</td>
	<td></td>
	<td></td>
</tr>

<tr>
	<td>&nbsp;</td>
	<td></td>
	<td></td>
</tr>


<tr>
	<td>Street:</td>
	<td><?php echo $this->Form->input('Street', array('label' => ''));?></td>
	<td></td>
</tr>

<tr>
<td>Apt, Suite, Floor, etc.</td>
<td><?php echo $this->Form->input('Street2', array('label' => ''));?></td>
	<td></td>
</tr>

<tr>
<td>City</td>
<td><?php echo $this->Form->input('City', array('label' => ''));?></td>
	<td></td>
</tr>

<tr>
<td>State</td>
<td><?php echo $this->Form->input('State', array('label' => ''));?></td>
	<td></td>
</tr>

<tr>
<td>ZIP Code</td>
<td><?php echo $this->Form->input('ZIP', array('label' => ''));?></td>
	<td></td>
</tr>

<tr>
	<td>&nbsp;</td>
	<td></td>
	<td></td>
</tr>

<tr>
	<td>Not Employed:</td>
	<td><?php echo $this->Form->input('Unemployed', array('label' => '', 'type' => 'checkbox', 'checked' => 'checked'));?></td>
	<td></td>
</tr>

<tr>
	<td>Last Date Employed:</td>
	<td>
		<?php /*<input type="date" name="data[LastDateEmployed]" value="" />*/?>
			<?php echo $this->Form->input('LastDateEmployed', array('type' => 'date', 'label' => '', 'maxYear' => '2012'));?>
	</td>
	<td></td>
</tr>

<tr>
	<td>I would like to work for:</td>
	<td>
	<?php 

		echo $this->Form->select('Unemployed', 
			array('label' => '',array(
			'Home Health Agency',
			'A Family Member',
			'A Nursing Home',
			'A Hospital',
			'None of the Above'
		)
		));

?>
	</td>
	<td></td>
</tr>

<tr>
	<td>&nbsp;</td>
	<td></td>
	<td></td>
</tr>

<tr>
	<td>&nbsp;</td>
	<td>
	<?php 

		echo $this->Form->submit('Register');

?>
	</td>
	<td></td>
</tr>

</table>
<?php echo $this->Form->end();?>
</div>
<?php else:?>
<h2 class="page_header">No Partner Loaded</h2>

<?php endif;?>
