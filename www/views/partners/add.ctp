<div class="partners form">
<?php echo $this->Form->create('Partner');?>
	<fieldset>
		<legend><?php __('Add Partner'); ?></legend>
	<?php
		echo $this->Form->input('Name');
		echo $this->Form->input('ShortName');
		echo $this->Form->input('Description');
		echo $this->Form->input('Url');
		//echo $this->Form->input('Created');
		//echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Partners', true), array('action' => 'index'));?></li>
	</ul>
</div>
