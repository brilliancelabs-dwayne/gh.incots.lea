<div class="partners form">
<?php echo $this->Form->create('Partner');?>
	<fieldset>
		<legend><?php __('Edit Partner'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('Name');
		echo $this->Form->input('ShortName');
		echo $this->Form->input('Description');
		echo $this->Form->input('Url');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Partner.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Partner.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Partners', true), array('action' => 'index'));?></li>
	</ul>
</div>