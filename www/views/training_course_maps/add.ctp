<div class="trainingCourseMaps form">
<?php echo $this->Form->create('TrainingCourseMap');?>
	<fieldset>
		<legend><?php __('Add Training Course Map'); ?></legend>
	<?php
		echo $this->Form->input('training_course_id');
		echo $this->Form->input('MapOrder');
		echo $this->Form->input('MapLevel');
		echo $this->Form->input('training_course_module');
		echo $this->Form->input('MapElementName');
		echo $this->Form->input('map_element_id');
		echo $this->Form->input('resource');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Training Course Maps', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Training Courses', true), array('controller' => 'training_courses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Training Course', true), array('controller' => 'training_courses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Map Elements', true), array('controller' => 'map_elements', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Map Element', true), array('controller' => 'map_elements', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Training Course Map Completions', true), array('controller' => 'training_course_map_completions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Training Course Map Completion', true), array('controller' => 'training_course_map_completions', 'action' => 'add')); ?> </li>
	</ul>
</div>