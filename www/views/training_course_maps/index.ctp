<div class="trainingCourseMaps index">
	<h2><?php __('Training Course Maps');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('training_course_id');?></th>
			<th><?php echo $this->Paginator->sort('MapOrder');?></th>
			<th><?php echo $this->Paginator->sort('MapLevel');?></th>
			<th><?php echo $this->Paginator->sort('training_course_module');?></th>
			<th><?php echo $this->Paginator->sort('MapElementName');?></th>
			<th><?php echo $this->Paginator->sort('map_element_id');?></th>
			<th><?php echo $this->Paginator->sort('resource');?></th>
			<th><?php echo $this->Paginator->sort('Updated');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($trainingCourseMaps as $trainingCourseMap):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $trainingCourseMap['TrainingCourseMap']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($trainingCourseMap['TrainingCourse']['CourseName'], array('controller' => 'training_courses', 'action' => 'view', $trainingCourseMap['TrainingCourse']['id'])); ?>
		</td>
		<td><?php echo $trainingCourseMap['TrainingCourseMap']['MapOrder']; ?>&nbsp;</td>
		<td><?php echo $trainingCourseMap['TrainingCourseMap']['MapLevel']; ?>&nbsp;</td>
		<td><?php echo $trainingCourseMap['TrainingCourseMap']['training_course_module']; ?>&nbsp;</td>
		<td><?php echo $trainingCourseMap['TrainingCourseMap']['MapElementName']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($trainingCourseMap['MapElement']['id'], array('controller' => 'map_elements', 'action' => 'view', $trainingCourseMap['MapElement']['id'])); ?>
		</td>
		<td><?php echo $trainingCourseMap['TrainingCourseMap']['resource']; ?>&nbsp;</td>
		<td><?php echo $trainingCourseMap['TrainingCourseMap']['Updated']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $trainingCourseMap['TrainingCourseMap']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $trainingCourseMap['TrainingCourseMap']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $trainingCourseMap['TrainingCourseMap']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $trainingCourseMap['TrainingCourseMap']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Training Course Map', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Training Courses', true), array('controller' => 'training_courses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Training Course', true), array('controller' => 'training_courses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Map Elements', true), array('controller' => 'map_elements', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Map Element', true), array('controller' => 'map_elements', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Training Course Map Completions', true), array('controller' => 'training_course_map_completions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Training Course Map Completion', true), array('controller' => 'training_course_map_completions', 'action' => 'add')); ?> </li>
	</ul>
</div>