<div class="trainingCourseMaps view">
<h2><?php  __('Training Course Map');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $trainingCourseMap['TrainingCourseMap']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Training Course'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($trainingCourseMap['TrainingCourse']['CourseName'], array('controller' => 'training_courses', 'action' => 'view', $trainingCourseMap['TrainingCourse']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('MapOrder'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $trainingCourseMap['TrainingCourseMap']['MapOrder']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('MapLevel'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $trainingCourseMap['TrainingCourseMap']['MapLevel']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Training Course Module'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $trainingCourseMap['TrainingCourseMap']['training_course_module']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('MapElementName'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $trainingCourseMap['TrainingCourseMap']['MapElementName']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Map Element'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($trainingCourseMap['MapElement']['id'], array('controller' => 'map_elements', 'action' => 'view', $trainingCourseMap['MapElement']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Resource'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $trainingCourseMap['TrainingCourseMap']['resource']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $trainingCourseMap['TrainingCourseMap']['Updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Training Course Map', true), array('action' => 'edit', $trainingCourseMap['TrainingCourseMap']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Training Course Map', true), array('action' => 'delete', $trainingCourseMap['TrainingCourseMap']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $trainingCourseMap['TrainingCourseMap']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Training Course Maps', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Training Course Map', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Training Courses', true), array('controller' => 'training_courses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Training Course', true), array('controller' => 'training_courses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Map Elements', true), array('controller' => 'map_elements', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Map Element', true), array('controller' => 'map_elements', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Training Course Map Completions', true), array('controller' => 'training_course_map_completions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Training Course Map Completion', true), array('controller' => 'training_course_map_completions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Training Course Map Completions');?></h3>
	<?php if (!empty($trainingCourseMap['TrainingCourseMapCompletion'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('User Id'); ?></th>
		<th><?php __('Training Course Map Id'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($trainingCourseMap['TrainingCourseMapCompletion'] as $trainingCourseMapCompletion):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $trainingCourseMapCompletion['id'];?></td>
			<td><?php echo $trainingCourseMapCompletion['user_id'];?></td>
			<td><?php echo $trainingCourseMapCompletion['training_course_map_id'];?></td>
			<td><?php echo $trainingCourseMapCompletion['Updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'training_course_map_completions', 'action' => 'view', $trainingCourseMapCompletion['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'training_course_map_completions', 'action' => 'edit', $trainingCourseMapCompletion['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'training_course_map_completions', 'action' => 'delete', $trainingCourseMapCompletion['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $trainingCourseMapCompletion['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Training Course Map Completion', true), array('controller' => 'training_course_map_completions', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
