<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
	//This fix will create a variable which can be checked before loaded content-specific layout structures
	$isModule = true;
	//module refers to whether or not a training module has been loaded
	if(isset($this->params['pass'][0])):
		$action = $this->params['pass'][0];
		if(!is_numeric($action)):
			$isModule = false;

		endif;

	endif;
?>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php __('Online Training Certification: Powered by Incots | '); ?>
		<?php echo $title_for_layout; ?>
	</title>

		<link href="/img/in_icon.png" type="image/x-icon" rel="icon" />
		<link href="/img/in_icon.png" type="image/x-icon" rel="shortcut icon" />
	<?php
		//echo $this->Html->meta('in_icon.png');

		echo $this->Html->css(array(
		'default', 
      'train',
		//'jquerytools/dateinput/skin1'
		));

		/**Load Partner-specific style*
		if(isset($user['partner_id'])){
			$file = "partners/".$user['partner_id'];
			echo $this->Html->css($file);
		}
		*/
		echo $this->Html->script(array(
			'jquery-latest', 
			'default'
			//'http://cdn.jquerytools.org/1.2.6/full/jquery.tools.min.js'

		));

		echo $scripts_for_layout;
	?>
</head>
<body>
<!--[LO: Default]-->
<style type="text/css">
<?php 
$bg = '/img/stock/orange.jpg';
?>
<!--
<?php if(isset($bg)):?>
	body {

		background: url(<?php echo $bg;?>);
		background-size: 100%
		background-color: black;
		color: white;
	}
	.login_box{
		
		
	}

<?php endif;?>
-->
</style>
<div id="container">
	<div id="header">
	<table class="header">
	<tr><td>
		<?php if(isset($user['partner_id'])):?>
			<h1><?php echo $partner;?></h1>
		<?php else:?>
			<?php /*<h1><span class="highlight">in</span>cots</h1>*/?>
			<img src= '/img/logo_white.png' width='70px' />
		<?php endif;?>
	</td>
	<td>
			<div class="status">
			<div class="displayName">
			<?php if(isset($user['FirstName']) && isset($user['LastName'])):?>
			<?php echo $user['FirstName']." ".$user['LastName'];?>
			<?php else:?>
			Sign	
			<?php endif;?>
			</div>	
			<?php if(isset($user['id'])){ 
				$in='notpicked'; 
				$out='picked';
			}else{ 
				$in='picked'; 
				$out='notpicked';
			}?>
<?php
if($in == 'picked'):?>
				<span class="in rounded <?php echo $in;?>"><a href="/train">in</a></span>
<?php elseif($out == 'picked'):?>
				<span class="out rounded <?php echo $out;?>"><a href="/users/logout">out</a></span>
<?php endif;?>
			</div>
	</td></tr>
	</table>
	</div>
	<div id="content">

		<?php echo $this->Session->flash(); ?>

		<?php echo $content_for_layout; ?>

	</div>
<?php /*
	<div id="footer">
		<div class="poweredby">Brilliance</div>
	</div>
*/
?>
</div>

</body>
</html>
