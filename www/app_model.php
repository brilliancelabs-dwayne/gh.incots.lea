<?php
class AppModel extends Model {
	public function beforeSave(){
		$model = key($this->data);
		$this->data = $this->_setStamp($model,$this->data);
		return true;
	}

	/**
	** Utilities
	**/
	public function _setStamp($model = null, $data = array(), $stamp = 'Updated'){

		if(isset($data[$model]) && $stamp != 'Both'):
			//debug('Case 1');
			//Set only the requested timestamp field
			unset($data[$model][$stamp]);
			$data[$model][$stamp] = date('Y-m-d H:i:s');
		elseif(isset($data[$model]) && $stamp == 'Both'):
			//Set timestamp for Created and Updated Fields
			unset($data[$model]['Created']);
			unset($data[$model]['Updated']);
			$data[$model]['Updated'] = $data[$model]['Created'] = date('Y-m-d H:i:s');
		else:
			//Debug if needed
		
		endif;

		//Return the resultant data set (unaltered if params are not correct
		return $data;
	}

}
