<?php
/* UserVideoResponse Test cases generated on: 2012-05-07 20:51:01 : 1336416661*/
App::import('Model', 'UserVideoResponse');

class UserVideoResponseTestCase extends CakeTestCase {
	var $fixtures = array('app.user_video_response', 'app.user', 'app.partner', 'app.partner_trainee', 'app.video_resource');

	function startTest() {
		$this->UserVideoResponse =& ClassRegistry::init('UserVideoResponse');
	}

	function endTest() {
		unset($this->UserVideoResponse);
		ClassRegistry::flush();
	}

}
