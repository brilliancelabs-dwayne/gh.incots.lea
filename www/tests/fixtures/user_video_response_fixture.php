<?php
/* UserVideoResponse Fixture generated on: 2012-05-07 20:51:01 : 1336416661 */
class UserVideoResponseFixture extends CakeTestFixture {
	var $name = 'UserVideoResponse';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 18, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 12),
		'video_resource_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 7),
		'Response' => array('type' => 'string', 'null' => true, 'default' => '1', 'length' => 1, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Updated' => array('type' => 'timestamp', 'null' => false, 'default' => 'CURRENT_TIMESTAMP'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'MyISAM')
	);

	var $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'video_resource_id' => 1,
			'Response' => 'Lorem ipsum dolor sit ame',
			'Updated' => '1336416661'
		),
	);
}
