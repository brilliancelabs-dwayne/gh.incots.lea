<?php

	Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
	Router::connect('/train/*', array('controller' => 'train', 'action' => 'index'));
	Router::connect('/train/:action/*', array('controller' => 'train', 'action' => 'index'));

	Router::connect('/register', array('controller' => 'registrations', 'action' => 'add'));
