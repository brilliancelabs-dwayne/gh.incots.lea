<?php

	class TrainController extends AppController {

		var $name = 'Train';

		var $uses = array('User', 'Partner', 'TrainingCourse', 'TrainingCourseMap',
		//The resources
		//db resources are only needed for DB loaded elements
		'VideoResource', 'QuizResource',
		//The responses
		'UserAgreementResponse', 'UserNoticeResponse', 'UserReadingResponse', 'UserCaseStudyResponse', 'UserQuizResponse', 'UserVideoResponse'
		);

		var $map;

		function beforeFilter(){
	
			parent::beforeFilter();

			$this->Auth->deny('*');

			$videos = $this->VideoResource->find('list');

			//quizzes also contains the questions with answer options	
			$quizzes = $this->QuizResource->find('all');

			//Set full map
			$gm = $this->getMap();

			//send to all train views
			$this->set(compact('videos', 'quizzes', 'gm'));		

		}
		



		/**
		** The primary function for training
		**/		
		function index($element = 0){

			/**
			** If a specific element is not requested then go to initial course element
			** CHANGE this so that the defaul loaded element is the last-completed element + 1
			**/
			if($element == 0):
				$this->redirect(array(
					'action' => 'index', 1

				));
			endif;

			/**
			** Load the List of partners and send to the view
			** Not sure why this was done...
			**/
			$partners = $this->Partner->find('list');

			/**
			** This should NOT be hard coded.  Load course id into session then request
			** for the id here
			** $course_id = 8; //OHHAC-EES
			**/
			$course_id = 8; 

			$courses = $this->TrainingCourse->find('first', array('conditions' => array(
			
				'TrainingCourse.id' => $course_id	

			)));
	
			$courses = $courses['TrainingCourse'];

			$tcm = $this->TrainingCourseMap->find('all', array('conditions' => array(
				'TrainingCourseMap.training_course_id' => $course_id
				
			)));


			//Navigation
			$tcm_total = count($tcm);

			$tcm_current = $element;

			if($tcm_current>$tcm_total){

				//if the requested current module is more than the max then ask for the max instead
				$tcm_current = $tcm_total;
			}

				//prev and next -- set 'em
				$tcm_prev = $tcm_current;
				if($tcm_current>1){
					$tcm_prev = $tcm_current-1;
				}

				$tcm_next = $tcm_current;
				if($tcm_next<$tcm_total){
					$tcm_next = $tcm_current+1;
				}

				//Navigation > Find completions
				//Gather responses
				//NOTE: Use this to build a "completion map"
				//The responses array will need to be parsed in the view to find out if it has been completed
					//NOTE:  why?  bad coding?
				$map = $tcm;	
				$i = 0;		
		
				foreach($tcm as $m){
					
					//Set the navigational pointers of the array
					$action = $m['TrainingCourseMap']['map_element_id'];
					$rid = $m['TrainingCourseMap']['resource'];

					//This builds a completion map based on the current training course map structure
					if($this->isComplete($action, $rid)){
						$map[$i]['TrainingCourseMap']['Completed'] = 1;
					}else{
						$map[$i]['TrainingCourseMap']['Completed'] = 0;
					}

					//Find out what the current level is
						//Also set the highest completed previous level which is used for access restriction
						//currentLevel - prevLevel <= (must be) 1
						if($map[$i]['TrainingCourseMap']['Completed'] == 1){
							$highestOrder = $map[$i]['TrainingCourseMap']['MapOrder'];
							$prevLevel = $map[$i]['TrainingCourseMap']['MapLevel'];
							
							//See if the next map element exists
							if(isset($map[$i+1]['TrainingCourseMap']['MapOrder'])){
								$currentLevel = $map[$i+1]['TrainingCourseMap']['MapLevel'];
							}else{
								$currentLevel = $prevLevel;
							}
						}

					//Increment
					$i++;
				}


				//Completion Map is the tcm with completion checks
				$cmap = $map;
			
				//Will the map still work if it's transformed into a cmap?
				$tcm = $cmap;



			//Determine whether the requested element should be viewable
			/*Logic: Allow OnlyIF Requested - CurrentLevel <= 1
			$levelAccess = 'deny';
			if(!isset($currentLevel)){$currentLevel=1;}
			$request = $tcm_current - $currentLevel;
			if($request<= 1){$levelAccess = 'allow';}
			*/
/****
**
** 
*****/
	//Determine whether the requested element should be viewable

			$highestCompletedLevel = 1; //initialize

			$levelAccess = 'deny'; //initialize

			$lowestIncompleteElement = null;  //initialize	
			if(isset($answerMap)){
				$numLevels = count($answerMap);
			}else{
				$answerMap = array();
				$numLevels = 0;
			}	
			for($i=1;$i<=$numLevels;$i++){

				$subLevels = count($answerMap[$i]);

				$completedSubs = null;


				foreach($answerMap[$i] as $s){
					$completedSubs+= $s;
					
					if($s=="" && !$lowestIncompleteElement){
						$lowestIncompleteElement = $i;
						//debug($answerMap[$i]);
					}
				}

				if($subLevels==$completedSubs){
					$highestCompletedLevel = $i;
				}

			}			

			if(!isset($currentLevel)){
				$currentLevel = $highestCompletedLevel+1; //initialize
			}

			// echo "<h2>Max Level $currentLevel</h2>";


		
			//The level of the element asked for in the App View
			$requestedLevel = $tcm[$element-1]['TrainingCourseMap']['MapLevel'];
			//echo "<h2>Requested Level $requestedLevel</h2>";		


			//The current level this user is working in (active but not completed)
			//The current level is in effect the access level
			$currentLevel;

			//The highest completed level is self-explanatory
			$highestCompletedLevel;
			
			//Current element is the highest incomplete element
			$currentElement = $lowestIncompleteElement+1;
			
			if($requestedLevel<=$currentLevel){
				$levelAccess = 'allow';
			}



/****
**
** 
*****/
			//send certain variable to views
			$this->set(compact('partners', 'courses', 'course_id', 'tcm', 
			//navi items
			'tcm_total', 'tcm_current', 'tcm_prev', 'tcm_next',
			//levels
			'currentLevel', 'levelAccess', 'highestCompletedLevel', 'requestedLevel', 'currentElement'
			
			));
		}

		function getMap(){
			$sql = 'SELECT mapOrder, MapLevel, MapElementName, me.Name, resource FROM `training_course_maps` INNER JOIN map_elements as me on map_element_id = me.id WHERE training_course_id = 8 limit 100';

			$getMap = $this->User->query( $sql );

			//debug( $getMap );

			$this->map = $getMap;

		}

		function isComplete($action = null, $rid = null){
	
			switch($action):
	
				case 1:
				$model = 'UserAgreementResponse';		
				$ridn = 'agreement_resource_id'; //resource id name
				break;

				case 2:
				$model = 'UserNoticeResponse';
				$ridn = 'notice_resource_id';
				break;

				case 3:
				$model = 'UserReadingResponse';
				$ridn = 'reading_resource_id';
				break;

				case 4:
				$model = 'UserCaseStudyResponse';
				$ridn = 'case_study_resource_id';
				break;

				case 5:
				$model = 'UserQuizResponse';
				$ridn = 'quiz_resource_id';
				break;

				case 6:
				$model = 'UserVideoResponse';
				$ridn = 'video_resource_id';
				break;

				default:
				$model = null;
				$ridn = null;
	
			endswitch;


			$result = $this->$model->find('first', array('conditions' => array(
				"user_id" => $this->user['id'],
				$ridn => $rid
			)));

			if(empty($result)){
				return false;
			}else{
				return true;
			}
		}

		function read($file = null){
			$this->layout = 'ajax';
			$base = "https://docs.google.com/viewer?url=";
			$filebase = "http://www.demaindtraining.org/webroot/OHHAC-EES/read/";
			$file = $filebase.$file."/";
			$url = $base.$file."&embedded=true";
			$readthis = $url;

			$this->set(compact('readthis'));
	
		}	
	}
