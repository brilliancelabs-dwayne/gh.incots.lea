<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Demand Training by Integrity Care Solutions</title>
<style type="text/css">
<!--
body,td,th {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 10pt;
	color: #000000;
}
body {
	background-color: #99CC00;
	background-image: url(img/bg_strip.png);
	background-repeat: repeat-x;
}
a:link {
	color: #000000;
	text-decoration: underline;
}
a:visited {
	text-decoration: underline;
	color: #000000;
}
a:hover {
	text-decoration: none;
	color: #99CC00;
}
a:active {
	text-decoration: underline;
	color: #99CC00;
}
h1,h2,h3,h4,h5,h6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
}
h1 {
	font-size: 16pt;
}
h2 {
	font-size: 18pt;
	color: #FF0000;
}
h3 {
	font-size: 18px;
}
.style1 {color: #FFFFFF}
.style2 {color: #FFFFFF; font-weight: bold; }
.style3 {color: #000000}
.style5 {font-size: 12px}
.style6 {
	font-size: 24pt;
	font-weight: bold;
	color: #000000;
}
-->
</style></head>

<body>
<table width="900" border="0" align="center" cellpadding="25" cellspacing="0">
  <tr>
    <td width="620" bgcolor="#FFFFFF"><img src="img/demand_training_logo_500.png" alt="Demand Training by Integrity Care Solutions" width="500" height="77" longdesc="http://www.demandtraining.com" /></td>
    <td width="265" align="right" valign="top" bgcolor="#FFFFFF"><div align="center">
      <table width="11%" height="100" border="1" cellpadding="0" cellspacing="0" bordercolor="#243000">
        <tr>
          <td height="98"><table width="250" cellpadding="7" cellspacing="0" bgcolor="#000000">
              <tr>
                <td colspan="2" bgcolor="#FFFFFF"><div align="right" class="style2">
                    <div align="left" class="style3">Account Login</div>
                </div></td>
              </tr>
              <tr>
                <td width="37%"><span class="style1">Username</span></td>
                <td width="63%"><input name="textfield" type="text" id="textfield" size="20" /></td>
              </tr>
              <tr>
                <td><span class="style1">Password</span></td>
                <td><input name="textfield2" type="text" id="textfield2" size="20" /></td>
              </tr>
              <tr>
                <td colspan="2"><div align="center" class="style1">
                    <input type="submit" name="button" id="button" value="Login" />
                </div></td>
              </tr>
          </table></td>
        </tr>
      </table>
    </div></td>
  </tr>
</table>
<table width="900" border="0" align="center" cellpadding="25" cellspacing="0">
  <tr>
    <td width="622" bgcolor="#FFFFFF"><h3>Overview</h3>
      <div align="justify" style="width:600px">Demand Training is a service line offered by Integrity Care  Solutions.  Demand Training allows for the  preparation and training of prospective employees so the trainee may receive a  needed skill base and/or certification in a specific discipline.  Currently, Integrity Care Solutions is able  to offer Demand Training for the Home Health Aide discipline.  STNA and C.N.A. curriculum will be activated  later this year.  Integrity Care  Solutions provides a mobile training solution by utilizing partnerships  throughout the region.  Currently,  training sessions are able to be offered on the east and west 
      sides of Cleveland.  In the summer of 2010 Integrity Care will provide  some training disciplines over the Internet.   Students will be able to learn various concepts while at home or in your  computer labs.  Skills will still be  taught in one of our mobile training solutions sites.      </div>
      <p>&nbsp;</p>
      <h3>Program Information and  Requirements:</h3>
      <ul>
        <li>Trainee Admittance: 20 minimum, 40 maximum</li>
        <li>Curriculum: Home Health Aide</li>
        <li>Deliverable: HHA Certification</li>
        <li>Cost per trainee*: $399.00</li>
        <li>Commitment Deposit per student: $40.00</li>
      </ul>
    <p><a href="more.php">Read more about the program</a> | <a href="http://www.ohioics.com" target="_blank">Go to our official web site</a></p>
    <p>&nbsp;</p>
    <h2>Get Started Today</h2>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p></td>
    <td width="263" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<table width="900" height="88" border="0" align="center" cellpadding="25" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td width="600" height="78" align="right" valign="middle"><div align="right" id="footer"><a href="#">About Us</a> | <a href="#">Contact Us</a> | <a href="#">Media</a> | <a href="#">Employment</a></div></td>
    <td width="200" align="right" valign="top"><div align="right">
      <p><img src="http://ohioics.com/index_files/nu_logo.gif" alt="http://ohioics.com/index_files/nu_logo.gif" width="200" /><br />
        <span class="style5">&copy; 2010 Integrity Care Solutions, Inc.</span></p>
      </div></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
