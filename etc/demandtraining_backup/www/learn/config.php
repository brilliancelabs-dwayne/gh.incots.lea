<?php  /// Moodle Configuration File

unset($CFG);

$CFG->dbtype    = 'mysql';
$CFG->dbhost    = 'mysql.omnis.com';
$CFG->dbname    = 'lends001_moodle_16889';
$CFG->dbuser    = 'moodle_16889';
$CFG->dbpass    = '4P6Tmp*b';
$CFG->dbpersist =  false;
$CFG->prefix    = 'mdl_';

$CFG->wwwroot   = 'https://demandtraining.secure.omnis.com/learn';
$CFG->dirroot   = '/webroot/l/e/lends001/demandtraining/www/learn';
$CFG->dataroot  = '/webroot/l/e/lends001/demandtraining/www/learn/datadir';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 00777;  // try 02777 on a server in Safe Mode

$CFG->passwordsaltmain = 's.><yl0XF+!k(Q~{wMoP8=VmtcfXK;~{';

require_once("$CFG->dirroot/lib/setup.php");
// MAKE SURE WHEN YOU EDIT THIS FILE THAT THERE ARE NO SPACES, BLANK LINES,
// RETURNS, OR ANYTHING ELSE AFTER THE TWO CHARACTERS ON THE NEXT LINE.
?>