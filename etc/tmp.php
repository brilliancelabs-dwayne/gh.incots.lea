<?php
		//Set the navigational pointers of the array
		$action = $m['TrainingCourseMap']['map_element_id'];
		$rid = $m['TrainingCourseMap']['resource'];

		//Build an AnswerMap
		$mapLevel = $m['TrainingCourseMap']['MapLevel'];
		$mapOrder = $m['TrainingCourseMap']['MapOrder'];
		$answerMap[$mapLevel][$mapOrder] = $this->isComplete($action, $rid);

		//This builds a completion map based on the current training course map structure
		if($this->isComplete($action, $rid)){
			$map[$i]['TrainingCourseMap']['Completed'] = 1;
		}else{
			$map[$i]['TrainingCourseMap']['Completed'] = 0;
		}


?>


<?php
	//Determine whether the requested element should be viewable

			$highestCompletedLevel = 1; //initialize

			$levelAccess = 'deny'; //initialize

			$lowestIncompleteElement = null;  //initialize	
			$numLevels = count($answerMap);	
			for($i=1;$i<=$numLevels;$i++){

				$subLevels = count($answerMap[$i]);

				$completedSubs = null;


				foreach($answerMap[$i] as $s){
					$completedSubs+= $s;
					
					if($s=="" && !$lowestIncompleteElement){
						$lowestIncompleteElement = $i;
						//debug($answerMap[$i]);
					}
				}

				if($subLevels==$completedSubs){
					$highestCompletedLevel = $i;
				}

			}			

			if(!isset($currentLevel)){
				$currentLevel = $highestCompletedLevel+1; //initialize
			}

			echo "<h2>Max Level $currentLevel</h2>";
/*	

			debug( $lowestIncompleteElement );
			debug( $currentLevel );
			debug( $highestCompletedLevel );
			debug( $answerMap );
*/

		
			//The level of the element asked for in the App View
			$requestedLevel = $tcm[$element-1]['TrainingCourseMap']['MapLevel'];
			echo "<h2>Requested Level $requestedLevel</h2>";		


			//The current level this user is working in (active but not completed)
			//The current level is in effect the access level
			$currentLevel;

			//The highest completed level is self-explanatory
			$highestCompletedLevel;
			
			//Current element is the highest incomplete element
			$currentElement = $lowestIncompleteElement+1;
			
			if($requestedLevel<=$currentLevel){
				$levelAccess = 'allow';
			}
			

			//send certain variable to views
			$this->set(compact('partners', 'courses', 'course_id', 'tcm', 
			//navi items
			'tcm_total', 'tcm_current', 'tcm_prev', 'tcm_next',
			//levels
			'currentLevel', 'levelAccess', 'highestCompletedLevel', 'requestedLevel', 'currentElement'
			));


?>
